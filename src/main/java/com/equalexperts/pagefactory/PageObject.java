package com.equalexperts.pagefactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public abstract class PageObject {
    protected WebDriver driver;
    protected WebDriverWait webDriverWait;

    public PageObject(WebDriver driver){
        this.driver = driver;
        webDriverWait = new WebDriverWait(driver,60);
        PageFactory.initElements(driver, this);
    }
    public abstract String navigate();

}