package com.equalexperts.pagefactory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import java.util.List;

public class HotelBookingForm extends PageObject{
    static Logger logger = LogManager.getLogger(HotelBookingForm.class);
    @FindBy(id="firstname")
    private WebElement firstNameInp;
    @FindBy(id="lastname")
    private WebElement surnameInp;
    @FindBy(id="totalprice")
    private WebElement priceInp;
    @FindBy(id="depositpaid")
    private WebElement depositInp;
    @FindBy(id="checkin")
    private WebElement checkinCal;
    @FindBy(id="checkout")
    private WebElement checkoutCal;
    @FindBy(xpath = "//input[@value=' Save ']")
    private WebElement saveBtn;

    private List<WebElement> bookingRecords;

    public HotelBookingForm(WebDriver driver) {
        super(driver);
    }
    public String navigate(){
        driver.get("http://hotel-test.equalexperts.io/");
        webDriverWait.until(ExpectedConditions.elementToBeClickable(By.id("firstname")));
        return driver.getTitle();
    }
    public String getTitle(){
        return driver.getTitle();
    }
    public void inputFirstName(String firstName){
       this.firstNameInp.sendKeys(firstName);
    }
    public void inputSurname(String surname){
        this.surnameInp.sendKeys(surname);
    }
    public void inputPrice(String price){
        this.priceInp.sendKeys(price);
    }
    public void inputDeposit(String deposit){
        this.depositInp.sendKeys(deposit);
    }
    public void inputCheckin(String checkin){
        this.checkinCal.sendKeys(checkin);
    }
    public void inputCheckout(String checkout){
        this.checkoutCal.sendKeys(checkout);
        this.firstNameInp.click();//as calender somtimes block the savebtn
    }
    public int clickSave(){
        try {
            int recordCount = countBookings();
            List<WebElement> earlierBookingRecords = bookingRecords;
            saveBtn.click();
            waitUntilPageInitialised();
            Thread.sleep(10000);
            if (recordCount < countBookings()) {
                for(int i=0; i<bookingRecords.size();i++){
                    if(!earlierBookingRecords.contains(bookingRecords.get(i))){
                        logger.debug("Added new booking successfully: "+bookingRecords.get(i).getAttribute("id"));
                        return (Integer.parseInt(bookingRecords.get(i).getAttribute("id")));
                    }
                }
            }
            return 0;
        }catch(Exception e){
            logger.error("Unable to add the booking" + e.toString());
            return 0;
        }
    }
    public void waitUntilPageInitialised(){
        try {
            webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@value=' Save ']")));
            webDriverWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value=' Save ']")));
        }catch (Exception e){
            logger.error(e.toString());
        }
    }
    public int countBookings(){
        try {
            waitUntilPageInitialised();
            //do it until we get the stable count
            do {
                bookingRecords = driver.findElements(By.xpath("//div[@id='bookings']/div[@id]"));
                Thread.sleep(1000);
            } while (bookingRecords.size() != driver.findElements(By.xpath("//div[@id='bookings']/div[@id]")).size());

            logger.debug("Total Bookings on the page: " + bookingRecords.size());
            for (int i = 0; i < bookingRecords.size(); i++) {
                logger.debug("id: " + bookingRecords.get(i).getAttribute("id"));
            }
            return bookingRecords.size();
        }catch(Exception e){
            logger.error("Error while counting elements: " + e.toString());
            return 0;
        }
    }
    public boolean bookingIdExists(int bookingId){
        List<WebElement> bookingRecord = driver.findElements(By.xpath("//div[@id='bookings']/div[@id='"+bookingId+"']//input[@value='Delete']"));
        if( bookingRecord.size()>0){
            logger.debug("Searched bookingId exists on screen: " + bookingRecord.get(0).getAttribute("id"));
            return true;
        }else{
            logger.debug("Searched bookingId does not exists: " + bookingId);
            return false;
        }
    }
    public boolean deleteBooking(int bookingId){
        List<WebElement> bookingRecord = driver.findElements(By.xpath("//div[@id='bookings']/div[@id='"+bookingId+"']//input[@value='Delete']"));
        if( bookingRecord.size()==1){
            logger.debug("deleting: " + bookingId);
            bookingRecord.get(0).click();
            webDriverWait.until(ExpectedConditions.stalenessOf(bookingRecord.get(0))); //wait till the element removed from dom
            //waitUntilPageInitialised();
            return true;
        }else{
            logger.error("multiple booking records exists for bookingId: " + bookingId);
            return false;
        }
    }
    public boolean deleteAllBooking() {
        waitUntilPageInitialised();
        countBookings();
        logger.debug("Deleting all Bookings on the page: " + bookingRecords.size());
        for (int i=0; i< bookingRecords.size();i++) {
            deleteBooking(Integer.parseInt(bookingRecords.get(i).getAttribute("id")));
        }
        return true;
    }

}
