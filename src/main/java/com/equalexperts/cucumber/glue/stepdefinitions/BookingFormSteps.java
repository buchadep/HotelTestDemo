package com.equalexperts.cucumber.glue.stepdefinitions;

import com.equalexperts.Utils;
import com.equalexperts.pagefactory.HotelBookingForm;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;



import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class BookingFormSteps {

    private WebDriver driver;
    private HotelBookingForm bookingForm;
    private int lastBookingId=0;
    private Properties config;
    private String driverType;
    public BookingFormSteps(){
        config = Utils.readConfigProperties(BookingFormSteps.class.getResource("/config.properties").getFile());
        driverType = config.getProperty("driver");
        if(driverType.equalsIgnoreCase("firefox")) {
            String driverPath = config.getProperty(driverType+".path");
            System.setProperty("webdriver.gecko.driver", driverPath + "geckodriver.exe");
            driver = new FirefoxDriver();
        }
        if(driverType.equalsIgnoreCase("chrome")) {
            String driverPath = config.getProperty(driverType+".path");
            System.setProperty("webdriver.chrome.driver", driverPath + "chromedriver.exe");
            driver = new ChromeDriver();
        }
        if(driverType.equalsIgnoreCase("InternetExplorer")) {
            String driverPath = config.getProperty(driverType+".path");
            System.setProperty("webdriver.ie.driver", driverPath + "IEDriverServer.exe");
            driver = new InternetExplorerDriver();
        }
        if(driverType.equalsIgnoreCase("phantomjs")) {
            String driverPath = config.getProperty(driverType+".path");
            System.setProperty("phantomjs.binary.path", driverPath + "phantomjs.exe");
            driver = new PhantomJSDriver();
        }
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }
    @Given("^booking form page opened in browser$")
    public void booking_form_page_opened_in_browser() throws Throwable {
        bookingForm = new HotelBookingForm(driver);
        bookingForm.navigate();
    }
    @When("^input Firstname as \"([^\"]*)\"$")
    public void input_firstname_as(String firstname) throws Throwable {
        this.bookingForm.inputFirstName(firstname);
    }
    @When("^input Surname as \"([^\"]*)\"$")
    public void input_surname_as(String surname) throws Throwable {
        this.bookingForm.inputSurname(surname);
    }
    @When("^input Price as \"([^\"]*)\"$")
    public void input_price_as(String price) throws Throwable {
        this.bookingForm.inputPrice(price);
    }
    @When("^input Deposit as \"([^\"]*)\"$")
    public void input_Deposit_as(String deposit) throws Throwable {
        this.bookingForm.inputDeposit(deposit);
    }
    @When("^input Check-in date as \"([^\"]*)\"$")
    public void input_Checkin_Date_as(String checkin) throws Throwable {
        if (checkin.toUpperCase().contains("$$TODAY")) {
            checkin = Utils.getTimeWrtNow(checkin);
        }
        bookingForm.inputCheckin(checkin);
    }
    @When("^input Check-out date as \"([^\"]*)\"$")
    public void input_Checkout_Date_as(String checkout) throws Throwable {
        if (checkout.toUpperCase().contains("$$TODAY")) {
            checkout = Utils.getTimeWrtNow(checkout);
        }
        bookingForm.inputCheckout(checkout);
    }
    @When("^save button clicked$")
    public void save_button_clicked() throws Throwable {
        lastBookingId=bookingForm.clickSave();
    }
    @When("^all bookings deleted$")
    public void all_bookings_deleted() throws Throwable {
        bookingForm.deleteAllBooking();
    }
    @When("^delete recently added booking$")
    public void delete_recently_added_booking() throws Throwable {
        bookingForm.deleteBooking(lastBookingId);
    }
    @Then("^booking successfully added$")
    public void booking_successsfully_added() throws Throwable {
        assertThat(lastBookingId,not(0));
    }
    @Then("^booking successfully deleted$")
    public void booking_successsfully_deleted() throws Throwable {
        assertThat("Deleting booking was not successful",bookingForm.bookingIdExists(lastBookingId),is(false));
    }
    @Then("^booking failed")
    public void booking_failed() throws Throwable {
        assertThat("Booking should not have saved successfully but saved with Id",lastBookingId,is(0));
    }
    @Then("^no bookings remaining on the page$")
    public void no_bookings_remaining_on_the_page() throws Throwable {
        assertThat("Booking Page still have bookings",Integer.toString(bookingForm.countBookings()),containsString("0"));
    }
    @Then("^booking form screen validation$")
    public void booking_form_screen_validation() throws Throwable {
        assertThat("Page Title is incorrect",bookingForm.getTitle(),containsString("Hotel booking form"));
    }
    @After
    @When("^close browser$")
    public void browser_closed() throws Throwable {
        driver.quit();
    }
}
