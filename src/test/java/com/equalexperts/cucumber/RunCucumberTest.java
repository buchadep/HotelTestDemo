package com.equalexperts.cucumber;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = { "pretty","html:target/reports/cucumber", "json:target/reports/cucumber/cucumber.json", "junit:target/reports/cucumber/cucumber-junit.xml"}
        ,glue = "com.equalexperts.cucumber.glue.stepdefinitions"
        ,features = "src\\test\\resources\\com\\equalexperts\\cucumber\\features"
        //,tags = "@ErrorFlow"
)
public class RunCucumberTest {
}
