Feature: Boundary value analysis - For fields - firastName,Surname,Price,Deposite,Checkoin,CheckOut. Please note that outside boundry failure tests are added under ErrorFlow feature.

  Background:
    Given booking form page opened in browser
  @BoundaryValueAnalysis
  Scenario Outline: BoundaryValueAnalysis - FirstName value analysis
    When input Firstname as "<firstName>"
    When input Surname as "FirastNameBA"
    When input Price as "11"
    When input Deposit as "false"
    When input Check-in date as "$$TODAY"
    When input Check-out date as "$$TODAY+99$DAYS"
    When save button clicked
    Then booking successfully added

  Examples:
    |firstName|
    |A         |
    |LENTHFGHIJABCDEFGHIJABCDEFGHIJABCDEFGHIJABCDEFGH50|

  @BoundaryValueAnalysis
  Scenario Outline: BoundaryValueAnalysis - LastName value analysis
    When input Firstname as "LastNameBA"
    When input Surname as "<lastName>"
    When input Price as "201"
    When input Deposit as "false"
    When input Check-in date as "$$TODAY"
    When input Check-out date as "$$TODAY+5$DAYS"
    When save button clicked
    Then booking successfully added

    Examples:
      |lastName|
      |A         |
      |LENTHFGHIJABCDEFGHIJABCDEFGHIJABCDEFGHIJABCDEFGH50|

  @BoundaryValueAnalysis
  Scenario Outline: BoundaryValueAnalysis - Price value analysis
    When input Firstname as "PriceBA"
    When input Surname as "PriceBA"
    When input Price as "<price>"
    When input Deposit as "false"
    When input Check-in date as "$$TODAY"
    When input Check-out date as "$$TODAY+1$DAYS"
    When save button clicked
    Then booking successfully added

    Examples:
      |price|
      |0|
      |0.00|
      |0.01|
      |0.1|
      |1.01|
      |99999|
      |99999.99|
      |99999.99999|

  @BoundaryValueAnalysis
  Scenario Outline: BoundaryValueAnalysis - Deposit value analysis
    When input Firstname as "DepositBA"
    When input Surname as "DepositBA"
    When input Price as "50.78"
    When input Deposit as "<deposit>"
    When input Check-in date as "$$TODAY"
    When input Check-out date as "$$TODAY+10$DAYS"
    When save button clicked
    Then booking successfully added

    Examples:
      |deposit|
      |true|
      |false|

  @BoundaryValueAnalysis
  Scenario Outline: BoundaryValueAnalysis - Checkin Date value analysis
    When input Firstname as "CheckinBA"
    When input Surname as "CheckinBA"
    When input Price as "60.55"
    When input Deposit as "true"
    When input Check-in date as "<checkin>"
    When input Check-out date as "$$TODAY+31$DAYS"
    When save button clicked
    Then booking successfully added

    Examples:
      |checkin|
      |$$TODAY+2$DAYS|
      |$$TODAY+25$DAYS|

  @BoundaryValueAnalysis
  Scenario Outline: BoundaryValueAnalysis - Checkout Date value analysis
    When input Firstname as "CheckoutBA"
    When input Surname as "CheckoutBA"
    When input Price as "33.55"
    When input Deposit as "true"
    When input Check-in date as "$$TODAY"
    When input Check-out date as "<checkout>"
    When save button clicked
    Then booking successfully added

    Examples:
      |checkout|
      |$$TODAY+2$DAYS|
      |$$TODAY+100$DAYS|