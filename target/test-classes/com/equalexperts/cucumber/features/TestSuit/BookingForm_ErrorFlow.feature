Feature: Error flow - when one of the logical constraint is violated after entering valid inputs,
  Booking system should not allow booking to be saved and appropriate error should be shown.

  Background:
    Given booking form page opened in browser

  @ErrorFlow
  Scenario: Duplicate booking - If user enters same data again and again, booking should not be saved
    When input Firstname as "DupTest"
    When input Surname as "DupTest"
    When input Price as "10"
    When input Deposit as "false"
    When input Check-in date as "$$TODAY"
    When input Check-out date as "$$TODAY"
    And save button clicked
    When input Firstname as "DupTest"
    When input Surname as "DupTest"
    When input Price as "10"
    When input Deposit as "false"
    When input Check-in date as "$$TODAY"
    When input Check-out date as "$$TODAY"
    And save button clicked
    Then booking failed

  @ErrorFlow
  Scenario: Check-out earlier than Check-in Date - If user enters Check-out date earlier than Check-in date, booking should not be saved
    When input Firstname as "ErrorChkInGTChkOut"
    When input Surname as "ErrorChkInGTChkOut"
    When input Price as "20.10"
    When input Deposit as "false"
    When input Check-in date as "$$TODAY"
    When input Check-out date as "$$TODAY-10$DAYS"
    And save button clicked
    Then booking failed

  @ErrorFlow
  Scenario: Special Char - in firstName and Surname not allowed, booking should not be saved
    When input Firstname as "Special ._#@&'$!<>? Char"
    When input Surname as "Special ._#@&'$!<>? Char"
    When input Price as "10"
    When input Deposit as "false"
    When input Check-in date as "2018-03-16"
    When input Check-out date as "2018-03-16"
    And save button clicked
    Then booking failed

  @ErrorFlow
  Scenario: Price less than zero - Negative price not allowed, booking should not be saved
    When input Firstname as "PriceNegative"
    When input Surname as "PriceNegative"
    When input Price as "-99"
    When input Deposit as "true"
    When input Check-in date as "$$TODAY"
    When input Check-out date as "$$TODAY"
    And save button clicked
    Then booking failed

  @ErrorFlow
  Scenario: Price multiple decimal point - multiple decimal point not allowed, booking should not be saved
    When input Firstname as "CharInPrice"
    When input Surname as "CharInPrice"
    When input Price as "1.00.0"
    When input Deposit as "true"
    When input Check-in date as "$$TODAY"
    When input Check-out date as "$$TODAY"
    And save button clicked
    Then booking failed

  @ErrorFlow
  Scenario: Price invalid number - Char price not allowed, booking should not be saved
    When input Firstname as "CharInPrice"
    When input Surname as "CharInPrice"
    When input Price as "A"
    When input Deposit as "true"
    When input Check-in date as "$$TODAY"
    When input Check-out date as "$$TODAY"
    And save button clicked
    Then booking failed


  @ErrorFlow
  Scenario: Empty FirstName field- empty firstName not allowed, booking should not be saved
    When input Firstname as ""
    When input Surname as "emptyFirstName"
    When input Price as "10"
    When input Deposit as "false"
    When input Check-in date as "$$TODAY"
    When input Check-out date as "$$TODAY"
    And save button clicked
    Then booking failed

  @ErrorFlow
  Scenario: Empty Surname field- empty Surname not allowed, booking should not be saved
    When input Firstname as "emptySurname"
    When input Surname as ""
    When input Price as "10"
    When input Deposit as "false"
    When input Check-in date as "$$TODAY"
    When input Check-out date as "$$TODAY"
    And save button clicked
    Then booking failed

  @ErrorFlow
  Scenario: Empty Price field - Price empty, booking should not be saved
    When input Firstname as "emptyPrice"
    When input Surname as "emptyPrice"
    When input Price as ""
    When input Deposit as "true"
    When input Check-in date as "$$TODAY"
    When input Check-out date as "$$TODAY"
    And save button clicked
    Then booking failed

  @ErrorFlow
  Scenario: Empty Chakin field - Checkin date not provided, booking should not be saved
    When input Firstname as "emptyCheckinDate"
    When input Surname as "emptyCheckinDate"
    When input Price as "99"
    When input Deposit as "true"
    When input Check-in date as ""
    When input Check-out date as "$$TODAY"
    And save button clicked
    Then booking failed

  @ErrorFlow
  Scenario: Empty Chakout field - Checkout date not provided, booking should not be saved
    When input Firstname as "emptyCheckoutDate"
    When input Surname as "emptyCheckoutDate"
    When input Price as "99"
    When input Deposit as "true"
    When input Check-in date as "$$TODAY"
    When input Check-out date as ""
    And save button clicked
    Then booking failed

  @ErrorFlow
  Scenario: Check-out and Check-in Date both in the past - if user enters Check-in and Checkout date in past, booking should not be saved
    When input Firstname as "ErrorChkInChkOutPast"
    When input Surname as "ErrorChkInChkOutPast"
    When input Price as "30"
    When input Deposit as "false"
    When input Check-in date as "$$TODAY-20$DAYS"
    When input Check-out date as "$$TODAY-9$DAYS"
    And save button clicked
    Then booking failed

  @ErrorFlow
  Scenario: Invalid Date Format CheckIn - if user enters incorrect format for Check-in, booking should not be saved
    When input Firstname as "InvalidCheckInDt"
    When input Surname as "InvalidCheckInDt"
    When input Price as "10.01"
    When input Deposit as "false"
    When input Check-in date as "2018-16-16"
    When input Check-out date as "2018-03-16"
    And save button clicked
    Then booking failed

  @ErrorFlow
  Scenario: Invalid Date Format Checkout - if user enters incorrect format for Check-out, booking should not be saved
    When input Firstname as "InvalidCheckOutDt"
    When input Surname as "InvalidCheckOutDt"
    When input Price as "10.02"
    When input Deposit as "true"
    When input Check-in date as "2018-03-16"
    When input Check-out date as "2018-16-16"
    And save button clicked
    Then booking failed

  @ErrorFlow
  Scenario: Invalid Date 31st Feb Checkout - if user enters incorrect format for Check-out, booking should not be saved
    When input Firstname as "31stFebCheckOut"
    When input Surname as "31stFebCheckOut"
    When input Price as "10.02"
    When input Deposit as "true"
    When input Check-in date as "2018-01-16"
    When input Check-out date as "2018-02-31"
    And save button clicked
    Then booking failed