Feature: Basic Flow - Save and Delete Bookings

  Background:
    Given booking form page opened in browser

  @BasicFlow
  Scenario: Booking Form Validation - Title, elements etc
    Then booking form screen validation

  @BasicFlow
  Scenario: Save Booking - Successfully added
    When input Firstname as "Pankaj"
    When input Surname as "Buchade"
    When input Price as "10.99"
    When input Deposit as "false"
    When input Check-in date as "$$TODAY"
    When input Check-out date as "$$TODAY+1$DAYS"
    And save button clicked
    Then booking successfully added
    And delete recently added booking

  @BasicFlow
  Scenario: Delete Booking - last booking successfully deleted after adding
    When input Firstname as "todelete"
    When input Surname as "todelete"
    When input Price as "30"
    When input Deposit as "false"
    When input Check-in date as "$$TODAY"
    When input Check-out date as "$$TODAY+15$DAYS"
    When save button clicked
    And delete recently added booking
    Then booking successfully deleted

  @BasicFlow
  Scenario: Delete all Bookings - user delete all bookings on the page no bookings shown on the page
    When all bookings deleted
    Then no bookings remaining on the page