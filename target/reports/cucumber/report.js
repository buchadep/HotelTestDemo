$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("TestSuit/BookingFormBoundaryValueTests.feature");
formatter.feature({
  "line": 1,
  "name": "Boundary value analysis - For fields - firastName,Surname,Price,Deposite,Checkoin,CheckOut. Please note that outside boundry failure tests are added under ErrorFlow feature.",
  "description": "",
  "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 6,
  "name": "BoundaryValueAnalysis - FirstName value analysis",
  "description": "",
  "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---firstname-value-analysis",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 5,
      "name": "@BoundaryValueAnalysis"
    }
  ]
});
formatter.step({
  "line": 7,
  "name": "input Firstname as \"\u003cfirstName\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "input Surname as \"FirastNameBA\"",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "input Price as \"11\"",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "input Deposit as \"false\"",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "input Check-in date as \"$$TODAY\"",
  "keyword": "When "
});
formatter.step({
  "line": 12,
  "name": "input Check-out date as \"$$TODAY+99$DAYS\"",
  "keyword": "When "
});
formatter.step({
  "line": 13,
  "name": "save button clicked",
  "keyword": "When "
});
formatter.step({
  "line": 14,
  "name": "booking successfully added",
  "keyword": "Then "
});
formatter.examples({
  "line": 16,
  "name": "",
  "description": "",
  "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---firstname-value-analysis;",
  "rows": [
    {
      "cells": [
        "firstName"
      ],
      "line": 17,
      "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---firstname-value-analysis;;1"
    },
    {
      "cells": [
        "A"
      ],
      "line": 18,
      "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---firstname-value-analysis;;2"
    },
    {
      "cells": [
        "LENTHFGHIJABCDEFGHIJABCDEFGHIJABCDEFGHIJABCDEFGH50"
      ],
      "line": 19,
      "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---firstname-value-analysis;;3"
    }
  ],
  "keyword": "Examples"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "booking form page opened in browser",
  "keyword": "Given "
});
formatter.match({
  "location": "BookingFormSteps.booking_form_page_opened_in_browser()"
});
formatter.result({
  "duration": 3871281476,
  "status": "passed"
});
formatter.scenario({
  "line": 18,
  "name": "BoundaryValueAnalysis - FirstName value analysis",
  "description": "",
  "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---firstname-value-analysis;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 5,
      "name": "@BoundaryValueAnalysis"
    }
  ]
});
formatter.step({
  "line": 7,
  "name": "input Firstname as \"A\"",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "input Surname as \"FirastNameBA\"",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "input Price as \"11\"",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "input Deposit as \"false\"",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "input Check-in date as \"$$TODAY\"",
  "keyword": "When "
});
formatter.step({
  "line": 12,
  "name": "input Check-out date as \"$$TODAY+99$DAYS\"",
  "keyword": "When "
});
formatter.step({
  "line": 13,
  "name": "save button clicked",
  "keyword": "When "
});
formatter.step({
  "line": 14,
  "name": "booking successfully added",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "A",
      "offset": 20
    }
  ],
  "location": "BookingFormSteps.input_firstname_as(String)"
});
formatter.result({
  "duration": 69981972,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "FirastNameBA",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_surname_as(String)"
});
formatter.result({
  "duration": 62593862,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "11",
      "offset": 16
    }
  ],
  "location": "BookingFormSteps.input_price_as(String)"
});
formatter.result({
  "duration": 68995598,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "false",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_Deposit_as(String)"
});
formatter.result({
  "duration": 48879658,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY",
      "offset": 24
    }
  ],
  "location": "BookingFormSteps.input_Checkin_Date_as(String)"
});
formatter.result({
  "duration": 123567335,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY+99$DAYS",
      "offset": 25
    }
  ],
  "location": "BookingFormSteps.input_Checkout_Date_as(String)"
});
formatter.result({
  "duration": 112750675,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.save_button_clicked()"
});
formatter.result({
  "duration": 12476453799,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.booking_successsfully_added()"
});
formatter.result({
  "duration": 3439194,
  "status": "passed"
});
formatter.after({
  "duration": 528931155,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "booking form page opened in browser",
  "keyword": "Given "
});
formatter.match({
  "location": "BookingFormSteps.booking_form_page_opened_in_browser()"
});
formatter.result({
  "duration": 2402694339,
  "status": "passed"
});
formatter.scenario({
  "line": 19,
  "name": "BoundaryValueAnalysis - FirstName value analysis",
  "description": "",
  "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---firstname-value-analysis;;3",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 5,
      "name": "@BoundaryValueAnalysis"
    }
  ]
});
formatter.step({
  "line": 7,
  "name": "input Firstname as \"LENTHFGHIJABCDEFGHIJABCDEFGHIJABCDEFGHIJABCDEFGH50\"",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "input Surname as \"FirastNameBA\"",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "input Price as \"11\"",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "input Deposit as \"false\"",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "input Check-in date as \"$$TODAY\"",
  "keyword": "When "
});
formatter.step({
  "line": 12,
  "name": "input Check-out date as \"$$TODAY+99$DAYS\"",
  "keyword": "When "
});
formatter.step({
  "line": 13,
  "name": "save button clicked",
  "keyword": "When "
});
formatter.step({
  "line": 14,
  "name": "booking successfully added",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "LENTHFGHIJABCDEFGHIJABCDEFGHIJABCDEFGHIJABCDEFGH50",
      "offset": 20
    }
  ],
  "location": "BookingFormSteps.input_firstname_as(String)"
});
formatter.result({
  "duration": 157550661,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "FirastNameBA",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_surname_as(String)"
});
formatter.result({
  "duration": 80133448,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "11",
      "offset": 16
    }
  ],
  "location": "BookingFormSteps.input_price_as(String)"
});
formatter.result({
  "duration": 68108432,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "false",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_Deposit_as(String)"
});
formatter.result({
  "duration": 54199614,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY",
      "offset": 24
    }
  ],
  "location": "BookingFormSteps.input_Checkin_Date_as(String)"
});
formatter.result({
  "duration": 151996181,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY+99$DAYS",
      "offset": 25
    }
  ],
  "location": "BookingFormSteps.input_Checkout_Date_as(String)"
});
formatter.result({
  "duration": 156244239,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.save_button_clicked()"
});
formatter.result({
  "duration": 12501222609,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.booking_successsfully_added()"
});
formatter.result({
  "duration": 31169,
  "status": "passed"
});
formatter.after({
  "duration": 528111268,
  "status": "passed"
});
formatter.scenarioOutline({
  "line": 22,
  "name": "BoundaryValueAnalysis - LastName value analysis",
  "description": "",
  "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---lastname-value-analysis",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 21,
      "name": "@BoundaryValueAnalysis"
    }
  ]
});
formatter.step({
  "line": 23,
  "name": "input Firstname as \"LastNameBA\"",
  "keyword": "When "
});
formatter.step({
  "line": 24,
  "name": "input Surname as \"\u003clastName\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 25,
  "name": "input Price as \"201\"",
  "keyword": "When "
});
formatter.step({
  "line": 26,
  "name": "input Deposit as \"false\"",
  "keyword": "When "
});
formatter.step({
  "line": 27,
  "name": "input Check-in date as \"$$TODAY\"",
  "keyword": "When "
});
formatter.step({
  "line": 28,
  "name": "input Check-out date as \"$$TODAY+5$DAYS\"",
  "keyword": "When "
});
formatter.step({
  "line": 29,
  "name": "save button clicked",
  "keyword": "When "
});
formatter.step({
  "line": 30,
  "name": "booking successfully added",
  "keyword": "Then "
});
formatter.examples({
  "line": 32,
  "name": "",
  "description": "",
  "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---lastname-value-analysis;",
  "rows": [
    {
      "cells": [
        "lastName"
      ],
      "line": 33,
      "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---lastname-value-analysis;;1"
    },
    {
      "cells": [
        "A"
      ],
      "line": 34,
      "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---lastname-value-analysis;;2"
    },
    {
      "cells": [
        "LENTHFGHIJABCDEFGHIJABCDEFGHIJABCDEFGHIJABCDEFGH50"
      ],
      "line": 35,
      "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---lastname-value-analysis;;3"
    }
  ],
  "keyword": "Examples"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "booking form page opened in browser",
  "keyword": "Given "
});
formatter.match({
  "location": "BookingFormSteps.booking_form_page_opened_in_browser()"
});
formatter.result({
  "duration": 2488475773,
  "status": "passed"
});
formatter.scenario({
  "line": 34,
  "name": "BoundaryValueAnalysis - LastName value analysis",
  "description": "",
  "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---lastname-value-analysis;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 21,
      "name": "@BoundaryValueAnalysis"
    }
  ]
});
formatter.step({
  "line": 23,
  "name": "input Firstname as \"LastNameBA\"",
  "keyword": "When "
});
formatter.step({
  "line": 24,
  "name": "input Surname as \"A\"",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 25,
  "name": "input Price as \"201\"",
  "keyword": "When "
});
formatter.step({
  "line": 26,
  "name": "input Deposit as \"false\"",
  "keyword": "When "
});
formatter.step({
  "line": 27,
  "name": "input Check-in date as \"$$TODAY\"",
  "keyword": "When "
});
formatter.step({
  "line": 28,
  "name": "input Check-out date as \"$$TODAY+5$DAYS\"",
  "keyword": "When "
});
formatter.step({
  "line": 29,
  "name": "save button clicked",
  "keyword": "When "
});
formatter.step({
  "line": 30,
  "name": "booking successfully added",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "LastNameBA",
      "offset": 20
    }
  ],
  "location": "BookingFormSteps.input_firstname_as(String)"
});
formatter.result({
  "duration": 76604549,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "A",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_surname_as(String)"
});
formatter.result({
  "duration": 85186569,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "201",
      "offset": 16
    }
  ],
  "location": "BookingFormSteps.input_price_as(String)"
});
formatter.result({
  "duration": 60348200,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "false",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_Deposit_as(String)"
});
formatter.result({
  "duration": 45299445,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY",
      "offset": 24
    }
  ],
  "location": "BookingFormSteps.input_Checkin_Date_as(String)"
});
formatter.result({
  "duration": 144604651,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY+5$DAYS",
      "offset": 25
    }
  ],
  "location": "BookingFormSteps.input_Checkout_Date_as(String)"
});
formatter.result({
  "duration": 134119062,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.save_button_clicked()"
});
formatter.result({
  "duration": 12515519895,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.booking_successsfully_added()"
});
formatter.result({
  "duration": 36490,
  "status": "passed"
});
formatter.after({
  "duration": 526111533,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "booking form page opened in browser",
  "keyword": "Given "
});
formatter.match({
  "location": "BookingFormSteps.booking_form_page_opened_in_browser()"
});
formatter.result({
  "duration": 2523195744,
  "status": "passed"
});
formatter.scenario({
  "line": 35,
  "name": "BoundaryValueAnalysis - LastName value analysis",
  "description": "",
  "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---lastname-value-analysis;;3",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 21,
      "name": "@BoundaryValueAnalysis"
    }
  ]
});
formatter.step({
  "line": 23,
  "name": "input Firstname as \"LastNameBA\"",
  "keyword": "When "
});
formatter.step({
  "line": 24,
  "name": "input Surname as \"LENTHFGHIJABCDEFGHIJABCDEFGHIJABCDEFGHIJABCDEFGH50\"",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 25,
  "name": "input Price as \"201\"",
  "keyword": "When "
});
formatter.step({
  "line": 26,
  "name": "input Deposit as \"false\"",
  "keyword": "When "
});
formatter.step({
  "line": 27,
  "name": "input Check-in date as \"$$TODAY\"",
  "keyword": "When "
});
formatter.step({
  "line": 28,
  "name": "input Check-out date as \"$$TODAY+5$DAYS\"",
  "keyword": "When "
});
formatter.step({
  "line": 29,
  "name": "save button clicked",
  "keyword": "When "
});
formatter.step({
  "line": 30,
  "name": "booking successfully added",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "LastNameBA",
      "offset": 20
    }
  ],
  "location": "BookingFormSteps.input_firstname_as(String)"
});
formatter.result({
  "duration": 91749850,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "LENTHFGHIJABCDEFGHIJABCDEFGHIJABCDEFGHIJABCDEFGH50",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_surname_as(String)"
});
formatter.result({
  "duration": 165195341,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "201",
      "offset": 16
    }
  ],
  "location": "BookingFormSteps.input_price_as(String)"
});
formatter.result({
  "duration": 60319691,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "false",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_Deposit_as(String)"
});
formatter.result({
  "duration": 50349526,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY",
      "offset": 24
    }
  ],
  "location": "BookingFormSteps.input_Checkin_Date_as(String)"
});
formatter.result({
  "duration": 121319012,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY+5$DAYS",
      "offset": 25
    }
  ],
  "location": "BookingFormSteps.input_Checkout_Date_as(String)"
});
formatter.result({
  "duration": 132131491,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.save_button_clicked()"
});
formatter.result({
  "duration": 12490107946,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.booking_successsfully_added()"
});
formatter.result({
  "duration": 23566,
  "status": "passed"
});
formatter.after({
  "duration": 525542895,
  "status": "passed"
});
formatter.scenarioOutline({
  "line": 38,
  "name": "BoundaryValueAnalysis - Price value analysis",
  "description": "",
  "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---price-value-analysis",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 37,
      "name": "@BoundaryValueAnalysis"
    }
  ]
});
formatter.step({
  "line": 39,
  "name": "input Firstname as \"PriceBA\"",
  "keyword": "When "
});
formatter.step({
  "line": 40,
  "name": "input Surname as \"PriceBA\"",
  "keyword": "When "
});
formatter.step({
  "line": 41,
  "name": "input Price as \"\u003cprice\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 42,
  "name": "input Deposit as \"false\"",
  "keyword": "When "
});
formatter.step({
  "line": 43,
  "name": "input Check-in date as \"$$TODAY\"",
  "keyword": "When "
});
formatter.step({
  "line": 44,
  "name": "input Check-out date as \"$$TODAY+1$DAYS\"",
  "keyword": "When "
});
formatter.step({
  "line": 45,
  "name": "save button clicked",
  "keyword": "When "
});
formatter.step({
  "line": 46,
  "name": "booking successfully added",
  "keyword": "Then "
});
formatter.examples({
  "line": 48,
  "name": "",
  "description": "",
  "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---price-value-analysis;",
  "rows": [
    {
      "cells": [
        "price"
      ],
      "line": 49,
      "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---price-value-analysis;;1"
    },
    {
      "cells": [
        "0"
      ],
      "line": 50,
      "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---price-value-analysis;;2"
    },
    {
      "cells": [
        "0.00"
      ],
      "line": 51,
      "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---price-value-analysis;;3"
    },
    {
      "cells": [
        "0.01"
      ],
      "line": 52,
      "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---price-value-analysis;;4"
    },
    {
      "cells": [
        "0.1"
      ],
      "line": 53,
      "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---price-value-analysis;;5"
    },
    {
      "cells": [
        "1.01"
      ],
      "line": 54,
      "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---price-value-analysis;;6"
    },
    {
      "cells": [
        "99999"
      ],
      "line": 55,
      "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---price-value-analysis;;7"
    },
    {
      "cells": [
        "99999.99"
      ],
      "line": 56,
      "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---price-value-analysis;;8"
    },
    {
      "cells": [
        "99999.99999"
      ],
      "line": 57,
      "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---price-value-analysis;;9"
    }
  ],
  "keyword": "Examples"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "booking form page opened in browser",
  "keyword": "Given "
});
formatter.match({
  "location": "BookingFormSteps.booking_form_page_opened_in_browser()"
});
formatter.result({
  "duration": 3111324519,
  "status": "passed"
});
formatter.scenario({
  "line": 50,
  "name": "BoundaryValueAnalysis - Price value analysis",
  "description": "",
  "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---price-value-analysis;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 37,
      "name": "@BoundaryValueAnalysis"
    }
  ]
});
formatter.step({
  "line": 39,
  "name": "input Firstname as \"PriceBA\"",
  "keyword": "When "
});
formatter.step({
  "line": 40,
  "name": "input Surname as \"PriceBA\"",
  "keyword": "When "
});
formatter.step({
  "line": 41,
  "name": "input Price as \"0\"",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 42,
  "name": "input Deposit as \"false\"",
  "keyword": "When "
});
formatter.step({
  "line": 43,
  "name": "input Check-in date as \"$$TODAY\"",
  "keyword": "When "
});
formatter.step({
  "line": 44,
  "name": "input Check-out date as \"$$TODAY+1$DAYS\"",
  "keyword": "When "
});
formatter.step({
  "line": 45,
  "name": "save button clicked",
  "keyword": "When "
});
formatter.step({
  "line": 46,
  "name": "booking successfully added",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "PriceBA",
      "offset": 20
    }
  ],
  "location": "BookingFormSteps.input_firstname_as(String)"
});
formatter.result({
  "duration": 51600453,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "PriceBA",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_surname_as(String)"
});
formatter.result({
  "duration": 56371917,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "0",
      "offset": 16
    }
  ],
  "location": "BookingFormSteps.input_price_as(String)"
});
formatter.result({
  "duration": 40995131,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "false",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_Deposit_as(String)"
});
formatter.result({
  "duration": 38303225,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY",
      "offset": 24
    }
  ],
  "location": "BookingFormSteps.input_Checkin_Date_as(String)"
});
formatter.result({
  "duration": 167041514,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY+1$DAYS",
      "offset": 25
    }
  ],
  "location": "BookingFormSteps.input_Checkout_Date_as(String)"
});
formatter.result({
  "duration": 139651497,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.save_button_clicked()"
});
formatter.result({
  "duration": 12475031444,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.booking_successsfully_added()"
});
formatter.result({
  "duration": 23187,
  "status": "passed"
});
formatter.after({
  "duration": 525283663,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "booking form page opened in browser",
  "keyword": "Given "
});
formatter.match({
  "location": "BookingFormSteps.booking_form_page_opened_in_browser()"
});
formatter.result({
  "duration": 2641118894,
  "status": "passed"
});
formatter.scenario({
  "line": 51,
  "name": "BoundaryValueAnalysis - Price value analysis",
  "description": "",
  "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---price-value-analysis;;3",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 37,
      "name": "@BoundaryValueAnalysis"
    }
  ]
});
formatter.step({
  "line": 39,
  "name": "input Firstname as \"PriceBA\"",
  "keyword": "When "
});
formatter.step({
  "line": 40,
  "name": "input Surname as \"PriceBA\"",
  "keyword": "When "
});
formatter.step({
  "line": 41,
  "name": "input Price as \"0.00\"",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 42,
  "name": "input Deposit as \"false\"",
  "keyword": "When "
});
formatter.step({
  "line": 43,
  "name": "input Check-in date as \"$$TODAY\"",
  "keyword": "When "
});
formatter.step({
  "line": 44,
  "name": "input Check-out date as \"$$TODAY+1$DAYS\"",
  "keyword": "When "
});
formatter.step({
  "line": 45,
  "name": "save button clicked",
  "keyword": "When "
});
formatter.step({
  "line": 46,
  "name": "booking successfully added",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "PriceBA",
      "offset": 20
    }
  ],
  "location": "BookingFormSteps.input_firstname_as(String)"
});
formatter.result({
  "duration": 51015851,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "PriceBA",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_surname_as(String)"
});
formatter.result({
  "duration": 45005623,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "0.00",
      "offset": 16
    }
  ],
  "location": "BookingFormSteps.input_price_as(String)"
});
formatter.result({
  "duration": 44377310,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "false",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_Deposit_as(String)"
});
formatter.result({
  "duration": 56281071,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY",
      "offset": 24
    }
  ],
  "location": "BookingFormSteps.input_Checkin_Date_as(String)"
});
formatter.result({
  "duration": 90488280,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY+1$DAYS",
      "offset": 25
    }
  ],
  "location": "BookingFormSteps.input_Checkout_Date_as(String)"
});
formatter.result({
  "duration": 113249753,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.save_button_clicked()"
});
formatter.result({
  "duration": 12454805654,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.booking_successsfully_added()"
});
formatter.result({
  "duration": 24327,
  "status": "passed"
});
formatter.after({
  "duration": 526259394,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "booking form page opened in browser",
  "keyword": "Given "
});
formatter.match({
  "location": "BookingFormSteps.booking_form_page_opened_in_browser()"
});
formatter.result({
  "duration": 2452766388,
  "status": "passed"
});
formatter.scenario({
  "line": 52,
  "name": "BoundaryValueAnalysis - Price value analysis",
  "description": "",
  "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---price-value-analysis;;4",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 37,
      "name": "@BoundaryValueAnalysis"
    }
  ]
});
formatter.step({
  "line": 39,
  "name": "input Firstname as \"PriceBA\"",
  "keyword": "When "
});
formatter.step({
  "line": 40,
  "name": "input Surname as \"PriceBA\"",
  "keyword": "When "
});
formatter.step({
  "line": 41,
  "name": "input Price as \"0.01\"",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 42,
  "name": "input Deposit as \"false\"",
  "keyword": "When "
});
formatter.step({
  "line": 43,
  "name": "input Check-in date as \"$$TODAY\"",
  "keyword": "When "
});
formatter.step({
  "line": 44,
  "name": "input Check-out date as \"$$TODAY+1$DAYS\"",
  "keyword": "When "
});
formatter.step({
  "line": 45,
  "name": "save button clicked",
  "keyword": "When "
});
formatter.step({
  "line": 46,
  "name": "booking successfully added",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "PriceBA",
      "offset": 20
    }
  ],
  "location": "BookingFormSteps.input_firstname_as(String)"
});
formatter.result({
  "duration": 61420477,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "PriceBA",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_surname_as(String)"
});
formatter.result({
  "duration": 45579963,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "0.01",
      "offset": 16
    }
  ],
  "location": "BookingFormSteps.input_price_as(String)"
});
formatter.result({
  "duration": 53183592,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "false",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_Deposit_as(String)"
});
formatter.result({
  "duration": 36686636,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY",
      "offset": 24
    }
  ],
  "location": "BookingFormSteps.input_Checkin_Date_as(String)"
});
formatter.result({
  "duration": 98514966,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY+1$DAYS",
      "offset": 25
    }
  ],
  "location": "BookingFormSteps.input_Checkout_Date_as(String)"
});
formatter.result({
  "duration": 112286567,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.save_button_clicked()"
});
formatter.result({
  "duration": 12488801524,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.booking_successsfully_added()"
});
formatter.result({
  "duration": 23567,
  "status": "passed"
});
formatter.after({
  "duration": 529410088,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "booking form page opened in browser",
  "keyword": "Given "
});
formatter.match({
  "location": "BookingFormSteps.booking_form_page_opened_in_browser()"
});
formatter.result({
  "duration": 2520772192,
  "status": "passed"
});
formatter.scenario({
  "line": 53,
  "name": "BoundaryValueAnalysis - Price value analysis",
  "description": "",
  "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---price-value-analysis;;5",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 37,
      "name": "@BoundaryValueAnalysis"
    }
  ]
});
formatter.step({
  "line": 39,
  "name": "input Firstname as \"PriceBA\"",
  "keyword": "When "
});
formatter.step({
  "line": 40,
  "name": "input Surname as \"PriceBA\"",
  "keyword": "When "
});
formatter.step({
  "line": 41,
  "name": "input Price as \"0.1\"",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 42,
  "name": "input Deposit as \"false\"",
  "keyword": "When "
});
formatter.step({
  "line": 43,
  "name": "input Check-in date as \"$$TODAY\"",
  "keyword": "When "
});
formatter.step({
  "line": 44,
  "name": "input Check-out date as \"$$TODAY+1$DAYS\"",
  "keyword": "When "
});
formatter.step({
  "line": 45,
  "name": "save button clicked",
  "keyword": "When "
});
formatter.step({
  "line": 46,
  "name": "booking successfully added",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "PriceBA",
      "offset": 20
    }
  ],
  "location": "BookingFormSteps.input_firstname_as(String)"
});
formatter.result({
  "duration": 59451911,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "PriceBA",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_surname_as(String)"
});
formatter.result({
  "duration": 54865179,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "0.1",
      "offset": 16
    }
  ],
  "location": "BookingFormSteps.input_price_as(String)"
});
formatter.result({
  "duration": 42076151,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "false",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_Deposit_as(String)"
});
formatter.result({
  "duration": 36572224,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY",
      "offset": 24
    }
  ],
  "location": "BookingFormSteps.input_Checkin_Date_as(String)"
});
formatter.result({
  "duration": 100167664,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY+1$DAYS",
      "offset": 25
    }
  ],
  "location": "BookingFormSteps.input_Checkout_Date_as(String)"
});
formatter.result({
  "duration": 110278849,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.save_button_clicked()"
});
formatter.result({
  "duration": 12497805841,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.booking_successsfully_added()"
});
formatter.result({
  "duration": 25467,
  "status": "passed"
});
formatter.after({
  "duration": 526419418,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "booking form page opened in browser",
  "keyword": "Given "
});
formatter.match({
  "location": "BookingFormSteps.booking_form_page_opened_in_browser()"
});
formatter.result({
  "duration": 2533609113,
  "status": "passed"
});
formatter.scenario({
  "line": 54,
  "name": "BoundaryValueAnalysis - Price value analysis",
  "description": "",
  "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---price-value-analysis;;6",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 37,
      "name": "@BoundaryValueAnalysis"
    }
  ]
});
formatter.step({
  "line": 39,
  "name": "input Firstname as \"PriceBA\"",
  "keyword": "When "
});
formatter.step({
  "line": 40,
  "name": "input Surname as \"PriceBA\"",
  "keyword": "When "
});
formatter.step({
  "line": 41,
  "name": "input Price as \"1.01\"",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 42,
  "name": "input Deposit as \"false\"",
  "keyword": "When "
});
formatter.step({
  "line": 43,
  "name": "input Check-in date as \"$$TODAY\"",
  "keyword": "When "
});
formatter.step({
  "line": 44,
  "name": "input Check-out date as \"$$TODAY+1$DAYS\"",
  "keyword": "When "
});
formatter.step({
  "line": 45,
  "name": "save button clicked",
  "keyword": "When "
});
formatter.step({
  "line": 46,
  "name": "booking successfully added",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "PriceBA",
      "offset": 20
    }
  ],
  "location": "BookingFormSteps.input_firstname_as(String)"
});
formatter.result({
  "duration": 57483345,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "PriceBA",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_surname_as(String)"
});
formatter.result({
  "duration": 55325487,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1.01",
      "offset": 16
    }
  ],
  "location": "BookingFormSteps.input_price_as(String)"
});
formatter.result({
  "duration": 40794816,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "false",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_Deposit_as(String)"
});
formatter.result({
  "duration": 45185033,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY",
      "offset": 24
    }
  ],
  "location": "BookingFormSteps.input_Checkin_Date_as(String)"
});
formatter.result({
  "duration": 90575704,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY+1$DAYS",
      "offset": 25
    }
  ],
  "location": "BookingFormSteps.input_Checkout_Date_as(String)"
});
formatter.result({
  "duration": 106451948,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.save_button_clicked()"
});
formatter.result({
  "duration": 12520924614,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.booking_successsfully_added()"
});
formatter.result({
  "duration": 25847,
  "status": "passed"
});
formatter.after({
  "duration": 536648436,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "booking form page opened in browser",
  "keyword": "Given "
});
formatter.match({
  "location": "BookingFormSteps.booking_form_page_opened_in_browser()"
});
formatter.result({
  "duration": 2450296843,
  "status": "passed"
});
formatter.scenario({
  "line": 55,
  "name": "BoundaryValueAnalysis - Price value analysis",
  "description": "",
  "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---price-value-analysis;;7",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 37,
      "name": "@BoundaryValueAnalysis"
    }
  ]
});
formatter.step({
  "line": 39,
  "name": "input Firstname as \"PriceBA\"",
  "keyword": "When "
});
formatter.step({
  "line": 40,
  "name": "input Surname as \"PriceBA\"",
  "keyword": "When "
});
formatter.step({
  "line": 41,
  "name": "input Price as \"99999\"",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 42,
  "name": "input Deposit as \"false\"",
  "keyword": "When "
});
formatter.step({
  "line": 43,
  "name": "input Check-in date as \"$$TODAY\"",
  "keyword": "When "
});
formatter.step({
  "line": 44,
  "name": "input Check-out date as \"$$TODAY+1$DAYS\"",
  "keyword": "When "
});
formatter.step({
  "line": 45,
  "name": "save button clicked",
  "keyword": "When "
});
formatter.step({
  "line": 46,
  "name": "booking successfully added",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "PriceBA",
      "offset": 20
    }
  ],
  "location": "BookingFormSteps.input_firstname_as(String)"
});
formatter.result({
  "duration": 50936789,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "PriceBA",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_surname_as(String)"
});
formatter.result({
  "duration": 44711042,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "99999",
      "offset": 16
    }
  ],
  "location": "BookingFormSteps.input_price_as(String)"
});
formatter.result({
  "duration": 68509443,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "false",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_Deposit_as(String)"
});
formatter.result({
  "duration": 35795289,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY",
      "offset": 24
    }
  ],
  "location": "BookingFormSteps.input_Checkin_Date_as(String)"
});
formatter.result({
  "duration": 90170132,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY+1$DAYS",
      "offset": 25
    }
  ],
  "location": "BookingFormSteps.input_Checkout_Date_as(String)"
});
formatter.result({
  "duration": 119753738,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.save_button_clicked()"
});
formatter.result({
  "duration": 12580694673,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.booking_successsfully_added()"
});
formatter.result({
  "duration": 25087,
  "status": "passed"
});
formatter.after({
  "duration": 526167788,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "booking form page opened in browser",
  "keyword": "Given "
});
formatter.match({
  "location": "BookingFormSteps.booking_form_page_opened_in_browser()"
});
formatter.result({
  "duration": 2527294421,
  "status": "passed"
});
formatter.scenario({
  "line": 56,
  "name": "BoundaryValueAnalysis - Price value analysis",
  "description": "",
  "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---price-value-analysis;;8",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 37,
      "name": "@BoundaryValueAnalysis"
    }
  ]
});
formatter.step({
  "line": 39,
  "name": "input Firstname as \"PriceBA\"",
  "keyword": "When "
});
formatter.step({
  "line": 40,
  "name": "input Surname as \"PriceBA\"",
  "keyword": "When "
});
formatter.step({
  "line": 41,
  "name": "input Price as \"99999.99\"",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 42,
  "name": "input Deposit as \"false\"",
  "keyword": "When "
});
formatter.step({
  "line": 43,
  "name": "input Check-in date as \"$$TODAY\"",
  "keyword": "When "
});
formatter.step({
  "line": 44,
  "name": "input Check-out date as \"$$TODAY+1$DAYS\"",
  "keyword": "When "
});
formatter.step({
  "line": 45,
  "name": "save button clicked",
  "keyword": "When "
});
formatter.step({
  "line": 46,
  "name": "booking successfully added",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "PriceBA",
      "offset": 20
    }
  ],
  "location": "BookingFormSteps.input_firstname_as(String)"
});
formatter.result({
  "duration": 52305168,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "PriceBA",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_surname_as(String)"
});
formatter.result({
  "duration": 53054737,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "99999.99",
      "offset": 16
    }
  ],
  "location": "BookingFormSteps.input_price_as(String)"
});
formatter.result({
  "duration": 51740332,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "false",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_Deposit_as(String)"
});
formatter.result({
  "duration": 35476001,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY",
      "offset": 24
    }
  ],
  "location": "BookingFormSteps.input_Checkin_Date_as(String)"
});
formatter.result({
  "duration": 88472580,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY+1$DAYS",
      "offset": 25
    }
  ],
  "location": "BookingFormSteps.input_Checkout_Date_as(String)"
});
formatter.result({
  "duration": 107432239,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.save_button_clicked()"
});
formatter.result({
  "duration": 12420978932,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.booking_successsfully_added()"
});
formatter.result({
  "duration": 26227,
  "status": "passed"
});
formatter.after({
  "duration": 525544035,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "booking form page opened in browser",
  "keyword": "Given "
});
formatter.match({
  "location": "BookingFormSteps.booking_form_page_opened_in_browser()"
});
formatter.result({
  "duration": 2578098173,
  "status": "passed"
});
formatter.scenario({
  "line": 57,
  "name": "BoundaryValueAnalysis - Price value analysis",
  "description": "",
  "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---price-value-analysis;;9",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 37,
      "name": "@BoundaryValueAnalysis"
    }
  ]
});
formatter.step({
  "line": 39,
  "name": "input Firstname as \"PriceBA\"",
  "keyword": "When "
});
formatter.step({
  "line": 40,
  "name": "input Surname as \"PriceBA\"",
  "keyword": "When "
});
formatter.step({
  "line": 41,
  "name": "input Price as \"99999.99999\"",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 42,
  "name": "input Deposit as \"false\"",
  "keyword": "When "
});
formatter.step({
  "line": 43,
  "name": "input Check-in date as \"$$TODAY\"",
  "keyword": "When "
});
formatter.step({
  "line": 44,
  "name": "input Check-out date as \"$$TODAY+1$DAYS\"",
  "keyword": "When "
});
formatter.step({
  "line": 45,
  "name": "save button clicked",
  "keyword": "When "
});
formatter.step({
  "line": 46,
  "name": "booking successfully added",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "PriceBA",
      "offset": 20
    }
  ],
  "location": "BookingFormSteps.input_firstname_as(String)"
});
formatter.result({
  "duration": 140538283,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "PriceBA",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_surname_as(String)"
});
formatter.result({
  "duration": 122707537,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "99999.99999",
      "offset": 16
    }
  ],
  "location": "BookingFormSteps.input_price_as(String)"
});
formatter.result({
  "duration": 137752490,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "false",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_Deposit_as(String)"
});
formatter.result({
  "duration": 91367844,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY",
      "offset": 24
    }
  ],
  "location": "BookingFormSteps.input_Checkin_Date_as(String)"
});
formatter.result({
  "duration": 243043216,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY+1$DAYS",
      "offset": 25
    }
  ],
  "location": "BookingFormSteps.input_Checkout_Date_as(String)"
});
formatter.result({
  "duration": 267032430,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.save_button_clicked()"
});
formatter.result({
  "duration": 12814808832,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.booking_successsfully_added()"
});
formatter.result({
  "duration": 39531,
  "status": "passed"
});
formatter.after({
  "duration": 535566656,
  "status": "passed"
});
formatter.scenarioOutline({
  "line": 60,
  "name": "BoundaryValueAnalysis - Deposit value analysis",
  "description": "",
  "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---deposit-value-analysis",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 59,
      "name": "@BoundaryValueAnalysis"
    }
  ]
});
formatter.step({
  "line": 61,
  "name": "input Firstname as \"DepositBA\"",
  "keyword": "When "
});
formatter.step({
  "line": 62,
  "name": "input Surname as \"DepositBA\"",
  "keyword": "When "
});
formatter.step({
  "line": 63,
  "name": "input Price as \"50.78\"",
  "keyword": "When "
});
formatter.step({
  "line": 64,
  "name": "input Deposit as \"\u003cdeposit\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 65,
  "name": "input Check-in date as \"$$TODAY\"",
  "keyword": "When "
});
formatter.step({
  "line": 66,
  "name": "input Check-out date as \"$$TODAY+10$DAYS\"",
  "keyword": "When "
});
formatter.step({
  "line": 67,
  "name": "save button clicked",
  "keyword": "When "
});
formatter.step({
  "line": 68,
  "name": "booking successfully added",
  "keyword": "Then "
});
formatter.examples({
  "line": 70,
  "name": "",
  "description": "",
  "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---deposit-value-analysis;",
  "rows": [
    {
      "cells": [
        "deposit"
      ],
      "line": 71,
      "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---deposit-value-analysis;;1"
    },
    {
      "cells": [
        "true"
      ],
      "line": 72,
      "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---deposit-value-analysis;;2"
    },
    {
      "cells": [
        "false"
      ],
      "line": 73,
      "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---deposit-value-analysis;;3"
    }
  ],
  "keyword": "Examples"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "booking form page opened in browser",
  "keyword": "Given "
});
formatter.match({
  "location": "BookingFormSteps.booking_form_page_opened_in_browser()"
});
formatter.result({
  "duration": 4432740153,
  "status": "passed"
});
formatter.scenario({
  "line": 72,
  "name": "BoundaryValueAnalysis - Deposit value analysis",
  "description": "",
  "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---deposit-value-analysis;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 59,
      "name": "@BoundaryValueAnalysis"
    }
  ]
});
formatter.step({
  "line": 61,
  "name": "input Firstname as \"DepositBA\"",
  "keyword": "When "
});
formatter.step({
  "line": 62,
  "name": "input Surname as \"DepositBA\"",
  "keyword": "When "
});
formatter.step({
  "line": 63,
  "name": "input Price as \"50.78\"",
  "keyword": "When "
});
formatter.step({
  "line": 64,
  "name": "input Deposit as \"true\"",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 65,
  "name": "input Check-in date as \"$$TODAY\"",
  "keyword": "When "
});
formatter.step({
  "line": 66,
  "name": "input Check-out date as \"$$TODAY+10$DAYS\"",
  "keyword": "When "
});
formatter.step({
  "line": 67,
  "name": "save button clicked",
  "keyword": "When "
});
formatter.step({
  "line": 68,
  "name": "booking successfully added",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "DepositBA",
      "offset": 20
    }
  ],
  "location": "BookingFormSteps.input_firstname_as(String)"
});
formatter.result({
  "duration": 76017666,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "DepositBA",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_surname_as(String)"
});
formatter.result({
  "duration": 59594830,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "50.78",
      "offset": 16
    }
  ],
  "location": "BookingFormSteps.input_price_as(String)"
});
formatter.result({
  "duration": 49849688,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "true",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_Deposit_as(String)"
});
formatter.result({
  "duration": 41171120,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY",
      "offset": 24
    }
  ],
  "location": "BookingFormSteps.input_Checkin_Date_as(String)"
});
formatter.result({
  "duration": 107627233,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY+10$DAYS",
      "offset": 25
    }
  ],
  "location": "BookingFormSteps.input_Checkout_Date_as(String)"
});
formatter.result({
  "duration": 133631767,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.save_button_clicked()"
});
formatter.result({
  "duration": 12621481506,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.booking_successsfully_added()"
});
formatter.result({
  "duration": 45233,
  "status": "passed"
});
formatter.after({
  "duration": 75825712,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "booking form page opened in browser",
  "keyword": "Given "
});
formatter.match({
  "location": "BookingFormSteps.booking_form_page_opened_in_browser()"
});
formatter.result({
  "duration": 2583867794,
  "status": "passed"
});
formatter.scenario({
  "line": 73,
  "name": "BoundaryValueAnalysis - Deposit value analysis",
  "description": "",
  "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---deposit-value-analysis;;3",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 59,
      "name": "@BoundaryValueAnalysis"
    }
  ]
});
formatter.step({
  "line": 61,
  "name": "input Firstname as \"DepositBA\"",
  "keyword": "When "
});
formatter.step({
  "line": 62,
  "name": "input Surname as \"DepositBA\"",
  "keyword": "When "
});
formatter.step({
  "line": 63,
  "name": "input Price as \"50.78\"",
  "keyword": "When "
});
formatter.step({
  "line": 64,
  "name": "input Deposit as \"false\"",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 65,
  "name": "input Check-in date as \"$$TODAY\"",
  "keyword": "When "
});
formatter.step({
  "line": 66,
  "name": "input Check-out date as \"$$TODAY+10$DAYS\"",
  "keyword": "When "
});
formatter.step({
  "line": 67,
  "name": "save button clicked",
  "keyword": "When "
});
formatter.step({
  "line": 68,
  "name": "booking successfully added",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "DepositBA",
      "offset": 20
    }
  ],
  "location": "BookingFormSteps.input_firstname_as(String)"
});
formatter.result({
  "duration": 127632183,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "DepositBA",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_surname_as(String)"
});
formatter.result({
  "duration": 67937384,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "50.78",
      "offset": 16
    }
  ],
  "location": "BookingFormSteps.input_price_as(String)"
});
formatter.result({
  "duration": 51790125,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "false",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_Deposit_as(String)"
});
formatter.result({
  "duration": 41285912,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY",
      "offset": 24
    }
  ],
  "location": "BookingFormSteps.input_Checkin_Date_as(String)"
});
formatter.result({
  "duration": 117926570,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY+10$DAYS",
      "offset": 25
    }
  ],
  "location": "BookingFormSteps.input_Checkout_Date_as(String)"
});
formatter.result({
  "duration": 143628540,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.save_button_clicked()"
});
formatter.result({
  "duration": 12704642114,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.booking_successsfully_added()"
});
formatter.result({
  "duration": 33830,
  "status": "passed"
});
formatter.after({
  "duration": 532724987,
  "status": "passed"
});
formatter.scenarioOutline({
  "line": 76,
  "name": "BoundaryValueAnalysis - Checkin Date value analysis",
  "description": "",
  "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---checkin-date-value-analysis",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 75,
      "name": "@BoundaryValueAnalysis"
    }
  ]
});
formatter.step({
  "line": 77,
  "name": "input Firstname as \"CheckinBA\"",
  "keyword": "When "
});
formatter.step({
  "line": 78,
  "name": "input Surname as \"CheckinBA\"",
  "keyword": "When "
});
formatter.step({
  "line": 79,
  "name": "input Price as \"60.55\"",
  "keyword": "When "
});
formatter.step({
  "line": 80,
  "name": "input Deposit as \"true\"",
  "keyword": "When "
});
formatter.step({
  "line": 81,
  "name": "input Check-in date as \"\u003ccheckin\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 82,
  "name": "input Check-out date as \"$$TODAY+31$DAYS\"",
  "keyword": "When "
});
formatter.step({
  "line": 83,
  "name": "save button clicked",
  "keyword": "When "
});
formatter.step({
  "line": 84,
  "name": "booking successfully added",
  "keyword": "Then "
});
formatter.examples({
  "line": 86,
  "name": "",
  "description": "",
  "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---checkin-date-value-analysis;",
  "rows": [
    {
      "cells": [
        "checkin"
      ],
      "line": 87,
      "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---checkin-date-value-analysis;;1"
    },
    {
      "cells": [
        "$$TODAY+2$DAYS"
      ],
      "line": 88,
      "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---checkin-date-value-analysis;;2"
    },
    {
      "cells": [
        "$$TODAY+25$DAYS"
      ],
      "line": 89,
      "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---checkin-date-value-analysis;;3"
    }
  ],
  "keyword": "Examples"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "booking form page opened in browser",
  "keyword": "Given "
});
formatter.match({
  "location": "BookingFormSteps.booking_form_page_opened_in_browser()"
});
formatter.result({
  "duration": 2390987852,
  "status": "passed"
});
formatter.scenario({
  "line": 88,
  "name": "BoundaryValueAnalysis - Checkin Date value analysis",
  "description": "",
  "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---checkin-date-value-analysis;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 75,
      "name": "@BoundaryValueAnalysis"
    }
  ]
});
formatter.step({
  "line": 77,
  "name": "input Firstname as \"CheckinBA\"",
  "keyword": "When "
});
formatter.step({
  "line": 78,
  "name": "input Surname as \"CheckinBA\"",
  "keyword": "When "
});
formatter.step({
  "line": 79,
  "name": "input Price as \"60.55\"",
  "keyword": "When "
});
formatter.step({
  "line": 80,
  "name": "input Deposit as \"true\"",
  "keyword": "When "
});
formatter.step({
  "line": 81,
  "name": "input Check-in date as \"$$TODAY+2$DAYS\"",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 82,
  "name": "input Check-out date as \"$$TODAY+31$DAYS\"",
  "keyword": "When "
});
formatter.step({
  "line": 83,
  "name": "save button clicked",
  "keyword": "When "
});
formatter.step({
  "line": 84,
  "name": "booking successfully added",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "CheckinBA",
      "offset": 20
    }
  ],
  "location": "BookingFormSteps.input_firstname_as(String)"
});
formatter.result({
  "duration": 61474832,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "CheckinBA",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_surname_as(String)"
});
formatter.result({
  "duration": 67820312,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "60.55",
      "offset": 16
    }
  ],
  "location": "BookingFormSteps.input_price_as(String)"
});
formatter.result({
  "duration": 57836463,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "true",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_Deposit_as(String)"
});
formatter.result({
  "duration": 49244940,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY+2$DAYS",
      "offset": 24
    }
  ],
  "location": "BookingFormSteps.input_Checkin_Date_as(String)"
});
formatter.result({
  "duration": 108655799,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY+31$DAYS",
      "offset": 25
    }
  ],
  "location": "BookingFormSteps.input_Checkout_Date_as(String)"
});
formatter.result({
  "duration": 121957969,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.save_button_clicked()"
});
formatter.result({
  "duration": 12658157880,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.booking_successsfully_added()"
});
formatter.result({
  "duration": 52454,
  "status": "passed"
});
formatter.after({
  "duration": 81260840,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "booking form page opened in browser",
  "keyword": "Given "
});
formatter.match({
  "location": "BookingFormSteps.booking_form_page_opened_in_browser()"
});
formatter.result({
  "duration": 2275721259,
  "status": "passed"
});
formatter.scenario({
  "line": 89,
  "name": "BoundaryValueAnalysis - Checkin Date value analysis",
  "description": "",
  "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---checkin-date-value-analysis;;3",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 75,
      "name": "@BoundaryValueAnalysis"
    }
  ]
});
formatter.step({
  "line": 77,
  "name": "input Firstname as \"CheckinBA\"",
  "keyword": "When "
});
formatter.step({
  "line": 78,
  "name": "input Surname as \"CheckinBA\"",
  "keyword": "When "
});
formatter.step({
  "line": 79,
  "name": "input Price as \"60.55\"",
  "keyword": "When "
});
formatter.step({
  "line": 80,
  "name": "input Deposit as \"true\"",
  "keyword": "When "
});
formatter.step({
  "line": 81,
  "name": "input Check-in date as \"$$TODAY+25$DAYS\"",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 82,
  "name": "input Check-out date as \"$$TODAY+31$DAYS\"",
  "keyword": "When "
});
formatter.step({
  "line": 83,
  "name": "save button clicked",
  "keyword": "When "
});
formatter.step({
  "line": 84,
  "name": "booking successfully added",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "CheckinBA",
      "offset": 20
    }
  ],
  "location": "BookingFormSteps.input_firstname_as(String)"
});
formatter.result({
  "duration": 85501677,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "CheckinBA",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_surname_as(String)"
});
formatter.result({
  "duration": 89435388,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "60.55",
      "offset": 16
    }
  ],
  "location": "BookingFormSteps.input_price_as(String)"
});
formatter.result({
  "duration": 74119420,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "true",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_Deposit_as(String)"
});
formatter.result({
  "duration": 56247242,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY+25$DAYS",
      "offset": 24
    }
  ],
  "location": "BookingFormSteps.input_Checkin_Date_as(String)"
});
formatter.result({
  "duration": 151406257,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY+31$DAYS",
      "offset": 25
    }
  ],
  "location": "BookingFormSteps.input_Checkout_Date_as(String)"
});
formatter.result({
  "duration": 140921429,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.save_button_clicked()"
});
formatter.result({
  "duration": 12715419243,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.booking_successsfully_added()"
});
formatter.result({
  "duration": 414315,
  "status": "passed"
});
formatter.after({
  "duration": 536700130,
  "status": "passed"
});
formatter.scenarioOutline({
  "line": 92,
  "name": "BoundaryValueAnalysis - Checkout Date value analysis",
  "description": "",
  "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---checkout-date-value-analysis",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 91,
      "name": "@BoundaryValueAnalysis"
    }
  ]
});
formatter.step({
  "line": 93,
  "name": "input Firstname as \"CheckoutBA\"",
  "keyword": "When "
});
formatter.step({
  "line": 94,
  "name": "input Surname as \"CheckoutBA\"",
  "keyword": "When "
});
formatter.step({
  "line": 95,
  "name": "input Price as \"33.55\"",
  "keyword": "When "
});
formatter.step({
  "line": 96,
  "name": "input Deposit as \"true\"",
  "keyword": "When "
});
formatter.step({
  "line": 97,
  "name": "input Check-in date as \"$$TODAY\"",
  "keyword": "When "
});
formatter.step({
  "line": 98,
  "name": "input Check-out date as \"\u003ccheckout\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 99,
  "name": "save button clicked",
  "keyword": "When "
});
formatter.step({
  "line": 100,
  "name": "booking successfully added",
  "keyword": "Then "
});
formatter.examples({
  "line": 102,
  "name": "",
  "description": "",
  "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---checkout-date-value-analysis;",
  "rows": [
    {
      "cells": [
        "checkout"
      ],
      "line": 103,
      "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---checkout-date-value-analysis;;1"
    },
    {
      "cells": [
        "$$TODAY+2$DAYS"
      ],
      "line": 104,
      "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---checkout-date-value-analysis;;2"
    },
    {
      "cells": [
        "$$TODAY+100$DAYS"
      ],
      "line": 105,
      "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---checkout-date-value-analysis;;3"
    }
  ],
  "keyword": "Examples"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "booking form page opened in browser",
  "keyword": "Given "
});
formatter.match({
  "location": "BookingFormSteps.booking_form_page_opened_in_browser()"
});
formatter.result({
  "duration": 2517501765,
  "status": "passed"
});
formatter.scenario({
  "line": 104,
  "name": "BoundaryValueAnalysis - Checkout Date value analysis",
  "description": "",
  "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---checkout-date-value-analysis;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 91,
      "name": "@BoundaryValueAnalysis"
    }
  ]
});
formatter.step({
  "line": 93,
  "name": "input Firstname as \"CheckoutBA\"",
  "keyword": "When "
});
formatter.step({
  "line": 94,
  "name": "input Surname as \"CheckoutBA\"",
  "keyword": "When "
});
formatter.step({
  "line": 95,
  "name": "input Price as \"33.55\"",
  "keyword": "When "
});
formatter.step({
  "line": 96,
  "name": "input Deposit as \"true\"",
  "keyword": "When "
});
formatter.step({
  "line": 97,
  "name": "input Check-in date as \"$$TODAY\"",
  "keyword": "When "
});
formatter.step({
  "line": 98,
  "name": "input Check-out date as \"$$TODAY+2$DAYS\"",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 99,
  "name": "save button clicked",
  "keyword": "When "
});
formatter.step({
  "line": 100,
  "name": "booking successfully added",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "CheckoutBA",
      "offset": 20
    }
  ],
  "location": "BookingFormSteps.input_firstname_as(String)"
});
formatter.result({
  "duration": 122819288,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "CheckoutBA",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_surname_as(String)"
});
formatter.result({
  "duration": 70687447,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "33.55",
      "offset": 16
    }
  ],
  "location": "BookingFormSteps.input_price_as(String)"
});
formatter.result({
  "duration": 52311630,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "true",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_Deposit_as(String)"
});
formatter.result({
  "duration": 59776141,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY",
      "offset": 24
    }
  ],
  "location": "BookingFormSteps.input_Checkin_Date_as(String)"
});
formatter.result({
  "duration": 110058768,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY+2$DAYS",
      "offset": 25
    }
  ],
  "location": "BookingFormSteps.input_Checkout_Date_as(String)"
});
formatter.result({
  "duration": 127891034,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.save_button_clicked()"
});
formatter.result({
  "duration": 12749431457,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.booking_successsfully_added()"
});
formatter.result({
  "duration": 48273,
  "status": "passed"
});
formatter.after({
  "duration": 532613997,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "booking form page opened in browser",
  "keyword": "Given "
});
formatter.match({
  "location": "BookingFormSteps.booking_form_page_opened_in_browser()"
});
formatter.result({
  "duration": 2493741754,
  "status": "passed"
});
formatter.scenario({
  "line": 105,
  "name": "BoundaryValueAnalysis - Checkout Date value analysis",
  "description": "",
  "id": "boundary-value-analysis---for-fields---firastname,surname,price,deposite,checkoin,checkout.-please-note-that-outside-boundry-failure-tests-are-added-under-errorflow-feature.;boundaryvalueanalysis---checkout-date-value-analysis;;3",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 91,
      "name": "@BoundaryValueAnalysis"
    }
  ]
});
formatter.step({
  "line": 93,
  "name": "input Firstname as \"CheckoutBA\"",
  "keyword": "When "
});
formatter.step({
  "line": 94,
  "name": "input Surname as \"CheckoutBA\"",
  "keyword": "When "
});
formatter.step({
  "line": 95,
  "name": "input Price as \"33.55\"",
  "keyword": "When "
});
formatter.step({
  "line": 96,
  "name": "input Deposit as \"true\"",
  "keyword": "When "
});
formatter.step({
  "line": 97,
  "name": "input Check-in date as \"$$TODAY\"",
  "keyword": "When "
});
formatter.step({
  "line": 98,
  "name": "input Check-out date as \"$$TODAY+100$DAYS\"",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 99,
  "name": "save button clicked",
  "keyword": "When "
});
formatter.step({
  "line": 100,
  "name": "booking successfully added",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "CheckoutBA",
      "offset": 20
    }
  ],
  "location": "BookingFormSteps.input_firstname_as(String)"
});
formatter.result({
  "duration": 69875542,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "CheckoutBA",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_surname_as(String)"
});
formatter.result({
  "duration": 62729180,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "33.55",
      "offset": 16
    }
  ],
  "location": "BookingFormSteps.input_price_as(String)"
});
formatter.result({
  "duration": 60452729,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "true",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_Deposit_as(String)"
});
formatter.result({
  "duration": 40833207,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY",
      "offset": 24
    }
  ],
  "location": "BookingFormSteps.input_Checkin_Date_as(String)"
});
formatter.result({
  "duration": 104942169,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY+100$DAYS",
      "offset": 25
    }
  ],
  "location": "BookingFormSteps.input_Checkout_Date_as(String)"
});
formatter.result({
  "duration": 126803553,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.save_button_clicked()"
});
formatter.result({
  "duration": 12750858373,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.booking_successsfully_added()"
});
formatter.result({
  "duration": 36110,
  "status": "passed"
});
formatter.after({
  "duration": 76234706,
  "status": "passed"
});
formatter.uri("TestSuit/BookingForm_BasicFlow.feature");
formatter.feature({
  "line": 1,
  "name": "Basic Flow - Save and Delete Bookings",
  "description": "",
  "id": "basic-flow---save-and-delete-bookings",
  "keyword": "Feature"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "booking form page opened in browser",
  "keyword": "Given "
});
formatter.match({
  "location": "BookingFormSteps.booking_form_page_opened_in_browser()"
});
formatter.result({
  "duration": 2918517938,
  "status": "passed"
});
formatter.scenario({
  "line": 7,
  "name": "Booking Form Validation - Title, elements etc",
  "description": "",
  "id": "basic-flow---save-and-delete-bookings;booking-form-validation---title,-elements-etc",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 6,
      "name": "@BasicFlow"
    }
  ]
});
formatter.step({
  "line": 8,
  "name": "booking form screen validation",
  "keyword": "Then "
});
formatter.match({
  "location": "BookingFormSteps.booking_form_screen_validation()"
});
formatter.result({
  "duration": 4027217,
  "status": "passed"
});
formatter.after({
  "duration": 534861179,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "booking form page opened in browser",
  "keyword": "Given "
});
formatter.match({
  "location": "BookingFormSteps.booking_form_page_opened_in_browser()"
});
formatter.result({
  "duration": 2448126822,
  "status": "passed"
});
formatter.scenario({
  "line": 11,
  "name": "Save Booking - Successfully added",
  "description": "",
  "id": "basic-flow---save-and-delete-bookings;save-booking---successfully-added",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 10,
      "name": "@BasicFlow"
    }
  ]
});
formatter.step({
  "line": 12,
  "name": "input Firstname as \"Pankaj\"",
  "keyword": "When "
});
formatter.step({
  "line": 13,
  "name": "input Surname as \"Buchade\"",
  "keyword": "When "
});
formatter.step({
  "line": 14,
  "name": "input Price as \"10.99\"",
  "keyword": "When "
});
formatter.step({
  "line": 15,
  "name": "input Deposit as \"false\"",
  "keyword": "When "
});
formatter.step({
  "line": 16,
  "name": "input Check-in date as \"$$TODAY\"",
  "keyword": "When "
});
formatter.step({
  "line": 17,
  "name": "input Check-out date as \"$$TODAY+1$DAYS\"",
  "keyword": "When "
});
formatter.step({
  "line": 18,
  "name": "save button clicked",
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "booking successfully added",
  "keyword": "Then "
});
formatter.step({
  "line": 20,
  "name": "delete recently added booking",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "Pankaj",
      "offset": 20
    }
  ],
  "location": "BookingFormSteps.input_firstname_as(String)"
});
formatter.result({
  "duration": 90570763,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Buchade",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_surname_as(String)"
});
formatter.result({
  "duration": 53110992,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "10.99",
      "offset": 16
    }
  ],
  "location": "BookingFormSteps.input_price_as(String)"
});
formatter.result({
  "duration": 50829599,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "false",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_Deposit_as(String)"
});
formatter.result({
  "duration": 48139592,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY",
      "offset": 24
    }
  ],
  "location": "BookingFormSteps.input_Checkin_Date_as(String)"
});
formatter.result({
  "duration": 120489241,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY+1$DAYS",
      "offset": 25
    }
  ],
  "location": "BookingFormSteps.input_Checkout_Date_as(String)"
});
formatter.result({
  "duration": 122284479,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.save_button_clicked()"
});
formatter.result({
  "duration": 12691730692,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.booking_successsfully_added()"
});
formatter.result({
  "duration": 26227,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.delete_recently_added_booking()"
});
formatter.result({
  "duration": 947442061,
  "status": "passed"
});
formatter.after({
  "duration": 560374616,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "booking form page opened in browser",
  "keyword": "Given "
});
formatter.match({
  "location": "BookingFormSteps.booking_form_page_opened_in_browser()"
});
formatter.result({
  "duration": 3041966681,
  "status": "passed"
});
formatter.scenario({
  "line": 23,
  "name": "Delete Booking - last booking successfully deleted after adding",
  "description": "",
  "id": "basic-flow---save-and-delete-bookings;delete-booking---last-booking-successfully-deleted-after-adding",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 22,
      "name": "@BasicFlow"
    }
  ]
});
formatter.step({
  "line": 24,
  "name": "input Firstname as \"todelete\"",
  "keyword": "When "
});
formatter.step({
  "line": 25,
  "name": "input Surname as \"todelete\"",
  "keyword": "When "
});
formatter.step({
  "line": 26,
  "name": "input Price as \"30\"",
  "keyword": "When "
});
formatter.step({
  "line": 27,
  "name": "input Deposit as \"false\"",
  "keyword": "When "
});
formatter.step({
  "line": 28,
  "name": "input Check-in date as \"$$TODAY\"",
  "keyword": "When "
});
formatter.step({
  "line": 29,
  "name": "input Check-out date as \"$$TODAY+15$DAYS\"",
  "keyword": "When "
});
formatter.step({
  "line": 30,
  "name": "save button clicked",
  "keyword": "When "
});
formatter.step({
  "line": 31,
  "name": "delete recently added booking",
  "keyword": "And "
});
formatter.step({
  "line": 32,
  "name": "booking successfully deleted",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "todelete",
      "offset": 20
    }
  ],
  "location": "BookingFormSteps.input_firstname_as(String)"
});
formatter.result({
  "duration": 96571867,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "todelete",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_surname_as(String)"
});
formatter.result({
  "duration": 52238650,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "30",
      "offset": 16
    }
  ],
  "location": "BookingFormSteps.input_price_as(String)"
});
formatter.result({
  "duration": 47842350,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "false",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_Deposit_as(String)"
});
formatter.result({
  "duration": 43519791,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY",
      "offset": 24
    }
  ],
  "location": "BookingFormSteps.input_Checkin_Date_as(String)"
});
formatter.result({
  "duration": 129068601,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY+15$DAYS",
      "offset": 25
    }
  ],
  "location": "BookingFormSteps.input_Checkout_Date_as(String)"
});
formatter.result({
  "duration": 138445422,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.save_button_clicked()"
});
formatter.result({
  "duration": 12703029706,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.delete_recently_added_booking()"
});
formatter.result({
  "duration": 909678206,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.booking_successsfully_deleted()"
});
formatter.result({
  "duration": 20019101438,
  "status": "passed"
});
formatter.after({
  "duration": 80961317,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "booking form page opened in browser",
  "keyword": "Given "
});
formatter.match({
  "location": "BookingFormSteps.booking_form_page_opened_in_browser()"
});
formatter.result({
  "duration": 2614687502,
  "status": "passed"
});
formatter.scenario({
  "line": 35,
  "name": "Delete all Bookings - user delete all bookings on the page no bookings shown on the page",
  "description": "",
  "id": "basic-flow---save-and-delete-bookings;delete-all-bookings---user-delete-all-bookings-on-the-page-no-bookings-shown-on-the-page",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 34,
      "name": "@BasicFlow"
    }
  ]
});
formatter.step({
  "line": 36,
  "name": "all bookings deleted",
  "keyword": "When "
});
formatter.step({
  "line": 37,
  "name": "no bookings remaining on the page",
  "keyword": "Then "
});
formatter.match({
  "location": "BookingFormSteps.all_bookings_deleted()"
});
formatter.result({
  "duration": 41194485887,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.no_bookings_remaining_on_the_page()"
});
formatter.result({
  "duration": 41165392617,
  "status": "passed"
});
formatter.after({
  "duration": 77188770,
  "status": "passed"
});
formatter.uri("TestSuit/BookingForm_ErrorFlow.feature");
formatter.feature({
  "line": 1,
  "name": "Error flow - when one of the logical constraint is violated after entering valid inputs,",
  "description": "Booking system should not allow booking to be saved and appropriate error should be shown.",
  "id": "error-flow---when-one-of-the-logical-constraint-is-violated-after-entering-valid-inputs,",
  "keyword": "Feature"
});
formatter.background({
  "line": 4,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "booking form page opened in browser",
  "keyword": "Given "
});
formatter.match({
  "location": "BookingFormSteps.booking_form_page_opened_in_browser()"
});
formatter.result({
  "duration": 2787824766,
  "status": "passed"
});
formatter.scenario({
  "line": 8,
  "name": "Duplicate booking - If user enters same data again and again, booking should not be saved",
  "description": "",
  "id": "error-flow---when-one-of-the-logical-constraint-is-violated-after-entering-valid-inputs,;duplicate-booking---if-user-enters-same-data-again-and-again,-booking-should-not-be-saved",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 7,
      "name": "@ErrorFlow"
    }
  ]
});
formatter.step({
  "line": 9,
  "name": "input Firstname as \"DupTest\"",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "input Surname as \"DupTest\"",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "input Price as \"10\"",
  "keyword": "When "
});
formatter.step({
  "line": 12,
  "name": "input Deposit as \"false\"",
  "keyword": "When "
});
formatter.step({
  "line": 13,
  "name": "input Check-in date as \"$$TODAY\"",
  "keyword": "When "
});
formatter.step({
  "line": 14,
  "name": "input Check-out date as \"$$TODAY\"",
  "keyword": "When "
});
formatter.step({
  "line": 15,
  "name": "save button clicked",
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "input Firstname as \"DupTest\"",
  "keyword": "When "
});
formatter.step({
  "line": 17,
  "name": "input Surname as \"DupTest\"",
  "keyword": "When "
});
formatter.step({
  "line": 18,
  "name": "input Price as \"10\"",
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "input Deposit as \"false\"",
  "keyword": "When "
});
formatter.step({
  "line": 20,
  "name": "input Check-in date as \"$$TODAY\"",
  "keyword": "When "
});
formatter.step({
  "line": 21,
  "name": "input Check-out date as \"$$TODAY\"",
  "keyword": "When "
});
formatter.step({
  "line": 22,
  "name": "save button clicked",
  "keyword": "And "
});
formatter.step({
  "line": 23,
  "name": "booking failed",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "DupTest",
      "offset": 20
    }
  ],
  "location": "BookingFormSteps.input_firstname_as(String)"
});
formatter.result({
  "duration": 108230461,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "DupTest",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_surname_as(String)"
});
formatter.result({
  "duration": 60608192,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "10",
      "offset": 16
    }
  ],
  "location": "BookingFormSteps.input_price_as(String)"
});
formatter.result({
  "duration": 54486594,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "false",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_Deposit_as(String)"
});
formatter.result({
  "duration": 40830546,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY",
      "offset": 24
    }
  ],
  "location": "BookingFormSteps.input_Checkin_Date_as(String)"
});
formatter.result({
  "duration": 118443894,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY",
      "offset": 25
    }
  ],
  "location": "BookingFormSteps.input_Checkout_Date_as(String)"
});
formatter.result({
  "duration": 127967436,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.save_button_clicked()"
});
formatter.result({
  "duration": 52338977646,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "DupTest",
      "offset": 20
    }
  ],
  "location": "BookingFormSteps.input_firstname_as(String)"
});
formatter.result({
  "duration": 33899323,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "DupTest",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_surname_as(String)"
});
formatter.result({
  "duration": 23830330,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "10",
      "offset": 16
    }
  ],
  "location": "BookingFormSteps.input_price_as(String)"
});
formatter.result({
  "duration": 21363445,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "false",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_Deposit_as(String)"
});
formatter.result({
  "duration": 18802295,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY",
      "offset": 24
    }
  ],
  "location": "BookingFormSteps.input_Checkin_Date_as(String)"
});
formatter.result({
  "duration": 54519662,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY",
      "offset": 25
    }
  ],
  "location": "BookingFormSteps.input_Checkout_Date_as(String)"
});
formatter.result({
  "duration": 72532859,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.save_button_clicked()"
});
formatter.result({
  "duration": 12227820753,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.booking_failed()"
});
formatter.result({
  "duration": 1549690,
  "error_message": "java.lang.AssertionError: Booking should not have saved successfully but saved with Id\nExpected: is \u003c0\u003e\n     but: was \u003c7529\u003e\r\n\tat org.hamcrest.MatcherAssert.assertThat(MatcherAssert.java:20)\r\n\tat org.junit.Assert.assertThat(Assert.java:865)\r\n\tat com.equalexperts.cucumber.glue.stepdefinitions.BookingFormSteps.booking_failed(BookingFormSteps.java:114)\r\n\tat ✽.Then booking failed(TestSuit/BookingForm_ErrorFlow.feature:23)\r\n",
  "status": "failed"
});
formatter.after({
  "duration": 543050171,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "booking form page opened in browser",
  "keyword": "Given "
});
formatter.match({
  "location": "BookingFormSteps.booking_form_page_opened_in_browser()"
});
formatter.result({
  "duration": 2435698894,
  "status": "passed"
});
formatter.scenario({
  "line": 26,
  "name": "Check-out earlier than Check-in Date - If user enters Check-out date earlier than Check-in date, booking should not be saved",
  "description": "",
  "id": "error-flow---when-one-of-the-logical-constraint-is-violated-after-entering-valid-inputs,;check-out-earlier-than-check-in-date---if-user-enters-check-out-date-earlier-than-check-in-date,-booking-should-not-be-saved",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 25,
      "name": "@ErrorFlow"
    }
  ]
});
formatter.step({
  "line": 27,
  "name": "input Firstname as \"ErrorChkInGTChkOut\"",
  "keyword": "When "
});
formatter.step({
  "line": 28,
  "name": "input Surname as \"ErrorChkInGTChkOut\"",
  "keyword": "When "
});
formatter.step({
  "line": 29,
  "name": "input Price as \"20.10\"",
  "keyword": "When "
});
formatter.step({
  "line": 30,
  "name": "input Deposit as \"false\"",
  "keyword": "When "
});
formatter.step({
  "line": 31,
  "name": "input Check-in date as \"$$TODAY\"",
  "keyword": "When "
});
formatter.step({
  "line": 32,
  "name": "input Check-out date as \"$$TODAY-10$DAYS\"",
  "keyword": "When "
});
formatter.step({
  "line": 33,
  "name": "save button clicked",
  "keyword": "And "
});
formatter.step({
  "line": 34,
  "name": "booking failed",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "ErrorChkInGTChkOut",
      "offset": 20
    }
  ],
  "location": "BookingFormSteps.input_firstname_as(String)"
});
formatter.result({
  "duration": 72339385,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "ErrorChkInGTChkOut",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_surname_as(String)"
});
formatter.result({
  "duration": 58861227,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "20.10",
      "offset": 16
    }
  ],
  "location": "BookingFormSteps.input_price_as(String)"
});
formatter.result({
  "duration": 44695077,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "false",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_Deposit_as(String)"
});
formatter.result({
  "duration": 49900241,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY",
      "offset": 24
    }
  ],
  "location": "BookingFormSteps.input_Checkin_Date_as(String)"
});
formatter.result({
  "duration": 96261321,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY-10$DAYS",
      "offset": 25
    }
  ],
  "location": "BookingFormSteps.input_Checkout_Date_as(String)"
});
formatter.result({
  "duration": 143073206,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.save_button_clicked()"
});
formatter.result({
  "duration": 12278014436,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.booking_failed()"
});
formatter.result({
  "duration": 195374,
  "error_message": "java.lang.AssertionError: Booking should not have saved successfully but saved with Id\nExpected: is \u003c0\u003e\n     but: was \u003c7531\u003e\r\n\tat org.hamcrest.MatcherAssert.assertThat(MatcherAssert.java:20)\r\n\tat org.junit.Assert.assertThat(Assert.java:865)\r\n\tat com.equalexperts.cucumber.glue.stepdefinitions.BookingFormSteps.booking_failed(BookingFormSteps.java:114)\r\n\tat ✽.Then booking failed(TestSuit/BookingForm_ErrorFlow.feature:34)\r\n",
  "status": "failed"
});
formatter.after({
  "duration": 526742508,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "booking form page opened in browser",
  "keyword": "Given "
});
formatter.match({
  "location": "BookingFormSteps.booking_form_page_opened_in_browser()"
});
formatter.result({
  "duration": 2118131448,
  "status": "passed"
});
formatter.scenario({
  "line": 37,
  "name": "Special Char - in firstName and Surname not allowed, booking should not be saved",
  "description": "",
  "id": "error-flow---when-one-of-the-logical-constraint-is-violated-after-entering-valid-inputs,;special-char---in-firstname-and-surname-not-allowed,-booking-should-not-be-saved",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 36,
      "name": "@ErrorFlow"
    }
  ]
});
formatter.step({
  "line": 38,
  "name": "input Firstname as \"Special ._#@\u0026\u0027$!\u003c\u003e? Char\"",
  "keyword": "When "
});
formatter.step({
  "line": 39,
  "name": "input Surname as \"Special ._#@\u0026\u0027$!\u003c\u003e? Char\"",
  "keyword": "When "
});
formatter.step({
  "line": 40,
  "name": "input Price as \"10\"",
  "keyword": "When "
});
formatter.step({
  "line": 41,
  "name": "input Deposit as \"false\"",
  "keyword": "When "
});
formatter.step({
  "line": 42,
  "name": "input Check-in date as \"2018-03-16\"",
  "keyword": "When "
});
formatter.step({
  "line": 43,
  "name": "input Check-out date as \"2018-03-16\"",
  "keyword": "When "
});
formatter.step({
  "line": 44,
  "name": "save button clicked",
  "keyword": "And "
});
formatter.step({
  "line": 45,
  "name": "booking failed",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Special ._#@\u0026\u0027$!\u003c\u003e? Char",
      "offset": 20
    }
  ],
  "location": "BookingFormSteps.input_firstname_as(String)"
});
formatter.result({
  "duration": 67123579,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Special ._#@\u0026\u0027$!\u003c\u003e? Char",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_surname_as(String)"
});
formatter.result({
  "duration": 61006542,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "10",
      "offset": 16
    }
  ],
  "location": "BookingFormSteps.input_price_as(String)"
});
formatter.result({
  "duration": 61805524,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "false",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_Deposit_as(String)"
});
formatter.result({
  "duration": 47717296,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "2018-03-16",
      "offset": 24
    }
  ],
  "location": "BookingFormSteps.input_Checkin_Date_as(String)"
});
formatter.result({
  "duration": 91381147,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "2018-03-16",
      "offset": 25
    }
  ],
  "location": "BookingFormSteps.input_Checkout_Date_as(String)"
});
formatter.result({
  "duration": 119117061,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.save_button_clicked()"
});
formatter.result({
  "duration": 12347071231,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.booking_failed()"
});
formatter.result({
  "duration": 194614,
  "error_message": "java.lang.AssertionError: Booking should not have saved successfully but saved with Id\nExpected: is \u003c0\u003e\n     but: was \u003c7532\u003e\r\n\tat org.hamcrest.MatcherAssert.assertThat(MatcherAssert.java:20)\r\n\tat org.junit.Assert.assertThat(Assert.java:865)\r\n\tat com.equalexperts.cucumber.glue.stepdefinitions.BookingFormSteps.booking_failed(BookingFormSteps.java:114)\r\n\tat ✽.Then booking failed(TestSuit/BookingForm_ErrorFlow.feature:45)\r\n",
  "status": "failed"
});
formatter.after({
  "duration": 535056934,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "booking form page opened in browser",
  "keyword": "Given "
});
formatter.match({
  "location": "BookingFormSteps.booking_form_page_opened_in_browser()"
});
formatter.result({
  "duration": 2226265741,
  "status": "passed"
});
formatter.scenario({
  "line": 48,
  "name": "Price less than zero - Negative price not allowed, booking should not be saved",
  "description": "",
  "id": "error-flow---when-one-of-the-logical-constraint-is-violated-after-entering-valid-inputs,;price-less-than-zero---negative-price-not-allowed,-booking-should-not-be-saved",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 47,
      "name": "@ErrorFlow"
    }
  ]
});
formatter.step({
  "line": 49,
  "name": "input Firstname as \"PriceNegative\"",
  "keyword": "When "
});
formatter.step({
  "line": 50,
  "name": "input Surname as \"PriceNegative\"",
  "keyword": "When "
});
formatter.step({
  "line": 51,
  "name": "input Price as \"-99\"",
  "keyword": "When "
});
formatter.step({
  "line": 52,
  "name": "input Deposit as \"true\"",
  "keyword": "When "
});
formatter.step({
  "line": 53,
  "name": "input Check-in date as \"$$TODAY\"",
  "keyword": "When "
});
formatter.step({
  "line": 54,
  "name": "input Check-out date as \"$$TODAY\"",
  "keyword": "When "
});
formatter.step({
  "line": 55,
  "name": "save button clicked",
  "keyword": "And "
});
formatter.step({
  "line": 56,
  "name": "booking failed",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "PriceNegative",
      "offset": 20
    }
  ],
  "location": "BookingFormSteps.input_firstname_as(String)"
});
formatter.result({
  "duration": 54995555,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "PriceNegative",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_surname_as(String)"
});
formatter.result({
  "duration": 51019652,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "-99",
      "offset": 16
    }
  ],
  "location": "BookingFormSteps.input_price_as(String)"
});
formatter.result({
  "duration": 47874279,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "true",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_Deposit_as(String)"
});
formatter.result({
  "duration": 36538015,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY",
      "offset": 24
    }
  ],
  "location": "BookingFormSteps.input_Checkin_Date_as(String)"
});
formatter.result({
  "duration": 100221640,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY",
      "offset": 25
    }
  ],
  "location": "BookingFormSteps.input_Checkout_Date_as(String)"
});
formatter.result({
  "duration": 112557581,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.save_button_clicked()"
});
formatter.result({
  "duration": 12357001105,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.booking_failed()"
});
formatter.result({
  "duration": 275576,
  "error_message": "java.lang.AssertionError: Booking should not have saved successfully but saved with Id\nExpected: is \u003c0\u003e\n     but: was \u003c7533\u003e\r\n\tat org.hamcrest.MatcherAssert.assertThat(MatcherAssert.java:20)\r\n\tat org.junit.Assert.assertThat(Assert.java:865)\r\n\tat com.equalexperts.cucumber.glue.stepdefinitions.BookingFormSteps.booking_failed(BookingFormSteps.java:114)\r\n\tat ✽.Then booking failed(TestSuit/BookingForm_ErrorFlow.feature:56)\r\n",
  "status": "failed"
});
formatter.after({
  "duration": 529987088,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "booking form page opened in browser",
  "keyword": "Given "
});
formatter.match({
  "location": "BookingFormSteps.booking_form_page_opened_in_browser()"
});
formatter.result({
  "duration": 2119771223,
  "status": "passed"
});
formatter.scenario({
  "line": 59,
  "name": "Price invalid number - Char price not allowed, booking should not be saved",
  "description": "",
  "id": "error-flow---when-one-of-the-logical-constraint-is-violated-after-entering-valid-inputs,;price-invalid-number---char-price-not-allowed,-booking-should-not-be-saved",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 58,
      "name": "@ErrorFlow"
    }
  ]
});
formatter.step({
  "line": 60,
  "name": "input Firstname as \"CharInPrice\"",
  "keyword": "When "
});
formatter.step({
  "line": 61,
  "name": "input Surname as \"CharInPrice\"",
  "keyword": "When "
});
formatter.step({
  "line": 62,
  "name": "input Price as \"A\"",
  "keyword": "When "
});
formatter.step({
  "line": 63,
  "name": "input Deposit as \"true\"",
  "keyword": "When "
});
formatter.step({
  "line": 64,
  "name": "input Check-in date as \"$$TODAY\"",
  "keyword": "When "
});
formatter.step({
  "line": 65,
  "name": "input Check-out date as \"$$TODAY\"",
  "keyword": "When "
});
formatter.step({
  "line": 66,
  "name": "save button clicked",
  "keyword": "And "
});
formatter.step({
  "line": 67,
  "name": "booking failed",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "CharInPrice",
      "offset": 20
    }
  ],
  "location": "BookingFormSteps.input_firstname_as(String)"
});
formatter.result({
  "duration": 57111982,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "CharInPrice",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_surname_as(String)"
});
formatter.result({
  "duration": 47874280,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "A",
      "offset": 16
    }
  ],
  "location": "BookingFormSteps.input_price_as(String)"
});
formatter.result({
  "duration": 47012200,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "true",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_Deposit_as(String)"
});
formatter.result({
  "duration": 44934544,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY",
      "offset": 24
    }
  ],
  "location": "BookingFormSteps.input_Checkin_Date_as(String)"
});
formatter.result({
  "duration": 104819775,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY",
      "offset": 25
    }
  ],
  "location": "BookingFormSteps.input_Checkout_Date_as(String)"
});
formatter.result({
  "duration": 150150768,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.save_button_clicked()"
});
formatter.result({
  "duration": 12344033048,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.booking_failed()"
});
formatter.result({
  "duration": 38011,
  "status": "passed"
});
formatter.after({
  "duration": 536615747,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "booking form page opened in browser",
  "keyword": "Given "
});
formatter.match({
  "location": "BookingFormSteps.booking_form_page_opened_in_browser()"
});
formatter.result({
  "duration": 2218053563,
  "status": "passed"
});
formatter.scenario({
  "line": 71,
  "name": "Empty FirstName field- empty firstName not allowed, booking should not be saved",
  "description": "",
  "id": "error-flow---when-one-of-the-logical-constraint-is-violated-after-entering-valid-inputs,;empty-firstname-field--empty-firstname-not-allowed,-booking-should-not-be-saved",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 70,
      "name": "@ErrorFlow"
    }
  ]
});
formatter.step({
  "line": 72,
  "name": "input Firstname as \"\"",
  "keyword": "When "
});
formatter.step({
  "line": 73,
  "name": "input Surname as \"emptyFirstName\"",
  "keyword": "When "
});
formatter.step({
  "line": 74,
  "name": "input Price as \"10\"",
  "keyword": "When "
});
formatter.step({
  "line": 75,
  "name": "input Deposit as \"false\"",
  "keyword": "When "
});
formatter.step({
  "line": 76,
  "name": "input Check-in date as \"$$TODAY\"",
  "keyword": "When "
});
formatter.step({
  "line": 77,
  "name": "input Check-out date as \"$$TODAY\"",
  "keyword": "When "
});
formatter.step({
  "line": 78,
  "name": "save button clicked",
  "keyword": "And "
});
formatter.step({
  "line": 79,
  "name": "booking failed",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "",
      "offset": 20
    }
  ],
  "location": "BookingFormSteps.input_firstname_as(String)"
});
formatter.result({
  "duration": 53610830,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "emptyFirstName",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_surname_as(String)"
});
formatter.result({
  "duration": 89094813,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "10",
      "offset": 16
    }
  ],
  "location": "BookingFormSteps.input_price_as(String)"
});
formatter.result({
  "duration": 54572877,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "false",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_Deposit_as(String)"
});
formatter.result({
  "duration": 35690380,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY",
      "offset": 24
    }
  ],
  "location": "BookingFormSteps.input_Checkin_Date_as(String)"
});
formatter.result({
  "duration": 94882678,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY",
      "offset": 25
    }
  ],
  "location": "BookingFormSteps.input_Checkout_Date_as(String)"
});
formatter.result({
  "duration": 110238558,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.save_button_clicked()"
});
formatter.result({
  "duration": 13204524471,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.booking_failed()"
});
formatter.result({
  "duration": 37250,
  "status": "passed"
});
formatter.after({
  "duration": 748925917,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "booking form page opened in browser",
  "keyword": "Given "
});
formatter.match({
  "location": "BookingFormSteps.booking_form_page_opened_in_browser()"
});
formatter.result({
  "duration": 3050117282,
  "status": "passed"
});
formatter.scenario({
  "line": 82,
  "name": "Empty Surname field- empty Surname not allowed, booking should not be saved",
  "description": "",
  "id": "error-flow---when-one-of-the-logical-constraint-is-violated-after-entering-valid-inputs,;empty-surname-field--empty-surname-not-allowed,-booking-should-not-be-saved",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 81,
      "name": "@ErrorFlow"
    }
  ]
});
formatter.step({
  "line": 83,
  "name": "input Firstname as \"emptySurname\"",
  "keyword": "When "
});
formatter.step({
  "line": 84,
  "name": "input Surname as \"\"",
  "keyword": "When "
});
formatter.step({
  "line": 85,
  "name": "input Price as \"10\"",
  "keyword": "When "
});
formatter.step({
  "line": 86,
  "name": "input Deposit as \"false\"",
  "keyword": "When "
});
formatter.step({
  "line": 87,
  "name": "input Check-in date as \"$$TODAY\"",
  "keyword": "When "
});
formatter.step({
  "line": 88,
  "name": "input Check-out date as \"$$TODAY\"",
  "keyword": "When "
});
formatter.step({
  "line": 89,
  "name": "save button clicked",
  "keyword": "And "
});
formatter.step({
  "line": 90,
  "name": "booking failed",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "emptySurname",
      "offset": 20
    }
  ],
  "location": "BookingFormSteps.input_firstname_as(String)"
});
formatter.result({
  "duration": 59483460,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_surname_as(String)"
});
formatter.result({
  "duration": 38632776,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "10",
      "offset": 16
    }
  ],
  "location": "BookingFormSteps.input_price_as(String)"
});
formatter.result({
  "duration": 45197577,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "false",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_Deposit_as(String)"
});
formatter.result({
  "duration": 46102608,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY",
      "offset": 24
    }
  ],
  "location": "BookingFormSteps.input_Checkin_Date_as(String)"
});
formatter.result({
  "duration": 90927681,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY",
      "offset": 25
    }
  ],
  "location": "BookingFormSteps.input_Checkout_Date_as(String)"
});
formatter.result({
  "duration": 105483059,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.save_button_clicked()"
});
formatter.result({
  "duration": 12385064669,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.booking_failed()"
});
formatter.result({
  "duration": 58536,
  "status": "passed"
});
formatter.after({
  "duration": 654220747,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "booking form page opened in browser",
  "keyword": "Given "
});
formatter.match({
  "location": "BookingFormSteps.booking_form_page_opened_in_browser()"
});
formatter.result({
  "duration": 3028451272,
  "status": "passed"
});
formatter.scenario({
  "line": 93,
  "name": "Empty Price field - Price empty, booking should not be saved",
  "description": "",
  "id": "error-flow---when-one-of-the-logical-constraint-is-violated-after-entering-valid-inputs,;empty-price-field---price-empty,-booking-should-not-be-saved",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 92,
      "name": "@ErrorFlow"
    }
  ]
});
formatter.step({
  "line": 94,
  "name": "input Firstname as \"emptyPrice\"",
  "keyword": "When "
});
formatter.step({
  "line": 95,
  "name": "input Surname as \"emptyPrice\"",
  "keyword": "When "
});
formatter.step({
  "line": 96,
  "name": "input Price as \"\"",
  "keyword": "When "
});
formatter.step({
  "line": 97,
  "name": "input Deposit as \"true\"",
  "keyword": "When "
});
formatter.step({
  "line": 98,
  "name": "input Check-in date as \"$$TODAY\"",
  "keyword": "When "
});
formatter.step({
  "line": 99,
  "name": "input Check-out date as \"$$TODAY\"",
  "keyword": "When "
});
formatter.step({
  "line": 100,
  "name": "save button clicked",
  "keyword": "And "
});
formatter.step({
  "line": 101,
  "name": "booking failed",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "emptyPrice",
      "offset": 20
    }
  ],
  "location": "BookingFormSteps.input_firstname_as(String)"
});
formatter.result({
  "duration": 78653697,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "emptyPrice",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_surname_as(String)"
});
formatter.result({
  "duration": 53596387,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "",
      "offset": 16
    }
  ],
  "location": "BookingFormSteps.input_price_as(String)"
});
formatter.result({
  "duration": 46905011,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "true",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_Deposit_as(String)"
});
formatter.result({
  "duration": 34792191,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY",
      "offset": 24
    }
  ],
  "location": "BookingFormSteps.input_Checkin_Date_as(String)"
});
formatter.result({
  "duration": 153267254,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY",
      "offset": 25
    }
  ],
  "location": "BookingFormSteps.input_Checkout_Date_as(String)"
});
formatter.result({
  "duration": 107735183,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.save_button_clicked()"
});
formatter.result({
  "duration": 12337847212,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.booking_failed()"
});
formatter.result({
  "duration": 36490,
  "status": "passed"
});
formatter.after({
  "duration": 526271177,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "booking form page opened in browser",
  "keyword": "Given "
});
formatter.match({
  "location": "BookingFormSteps.booking_form_page_opened_in_browser()"
});
formatter.result({
  "duration": 3192017482,
  "status": "passed"
});
formatter.scenario({
  "line": 104,
  "name": "Empty Chakin field - Checkin date not provided, booking should not be saved",
  "description": "",
  "id": "error-flow---when-one-of-the-logical-constraint-is-violated-after-entering-valid-inputs,;empty-chakin-field---checkin-date-not-provided,-booking-should-not-be-saved",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 103,
      "name": "@ErrorFlow"
    }
  ]
});
formatter.step({
  "line": 105,
  "name": "input Firstname as \"emptyCheckinDate\"",
  "keyword": "When "
});
formatter.step({
  "line": 106,
  "name": "input Surname as \"emptyCheckinDate\"",
  "keyword": "When "
});
formatter.step({
  "line": 107,
  "name": "input Price as \"99\"",
  "keyword": "When "
});
formatter.step({
  "line": 108,
  "name": "input Deposit as \"true\"",
  "keyword": "When "
});
formatter.step({
  "line": 109,
  "name": "input Check-in date as \"\"",
  "keyword": "When "
});
formatter.step({
  "line": 110,
  "name": "input Check-out date as \"$$TODAY\"",
  "keyword": "When "
});
formatter.step({
  "line": 111,
  "name": "save button clicked",
  "keyword": "And "
});
formatter.step({
  "line": 112,
  "name": "booking failed",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "emptyCheckinDate",
      "offset": 20
    }
  ],
  "location": "BookingFormSteps.input_firstname_as(String)"
});
formatter.result({
  "duration": 304290364,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "emptyCheckinDate",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_surname_as(String)"
});
formatter.result({
  "duration": 354887339,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "99",
      "offset": 16
    }
  ],
  "location": "BookingFormSteps.input_price_as(String)"
});
formatter.result({
  "duration": 240616242,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "true",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_Deposit_as(String)"
});
formatter.result({
  "duration": 110080814,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "",
      "offset": 24
    }
  ],
  "location": "BookingFormSteps.input_Checkin_Date_as(String)"
});
formatter.result({
  "duration": 148451317,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY",
      "offset": 25
    }
  ],
  "location": "BookingFormSteps.input_Checkout_Date_as(String)"
});
formatter.result({
  "duration": 352490014,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.save_button_clicked()"
});
formatter.result({
  "duration": 12680780995,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.booking_failed()"
});
formatter.result({
  "duration": 34589,
  "status": "passed"
});
formatter.after({
  "duration": 531406401,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "booking form page opened in browser",
  "keyword": "Given "
});
formatter.match({
  "location": "BookingFormSteps.booking_form_page_opened_in_browser()"
});
formatter.result({
  "duration": 2743134630,
  "status": "passed"
});
formatter.scenario({
  "line": 115,
  "name": "Empty Chakout field - Checkout date not provided, booking should not be saved",
  "description": "",
  "id": "error-flow---when-one-of-the-logical-constraint-is-violated-after-entering-valid-inputs,;empty-chakout-field---checkout-date-not-provided,-booking-should-not-be-saved",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 114,
      "name": "@ErrorFlow"
    }
  ]
});
formatter.step({
  "line": 116,
  "name": "input Firstname as \"emptyCheckoutDate\"",
  "keyword": "When "
});
formatter.step({
  "line": 117,
  "name": "input Surname as \"emptyCheckoutDate\"",
  "keyword": "When "
});
formatter.step({
  "line": 118,
  "name": "input Price as \"99\"",
  "keyword": "When "
});
formatter.step({
  "line": 119,
  "name": "input Deposit as \"true\"",
  "keyword": "When "
});
formatter.step({
  "line": 120,
  "name": "input Check-in date as \"$$TODAY\"",
  "keyword": "When "
});
formatter.step({
  "line": 121,
  "name": "input Check-out date as \"\"",
  "keyword": "When "
});
formatter.step({
  "line": 122,
  "name": "save button clicked",
  "keyword": "And "
});
formatter.step({
  "line": 123,
  "name": "booking failed",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "emptyCheckoutDate",
      "offset": 20
    }
  ],
  "location": "BookingFormSteps.input_firstname_as(String)"
});
formatter.result({
  "duration": 107433760,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "emptyCheckoutDate",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_surname_as(String)"
});
formatter.result({
  "duration": 62910490,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "99",
      "offset": 16
    }
  ],
  "location": "BookingFormSteps.input_price_as(String)"
});
formatter.result({
  "duration": 46854836,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "true",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_Deposit_as(String)"
});
formatter.result({
  "duration": 54806642,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY",
      "offset": 24
    }
  ],
  "location": "BookingFormSteps.input_Checkin_Date_as(String)"
});
formatter.result({
  "duration": 121583565,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "",
      "offset": 25
    }
  ],
  "location": "BookingFormSteps.input_Checkout_Date_as(String)"
});
formatter.result({
  "duration": 111705385,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.save_button_clicked()"
});
formatter.result({
  "duration": 12355605358,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.booking_failed()"
});
formatter.result({
  "duration": 33829,
  "status": "passed"
});
formatter.after({
  "duration": 528587539,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "booking form page opened in browser",
  "keyword": "Given "
});
formatter.match({
  "location": "BookingFormSteps.booking_form_page_opened_in_browser()"
});
formatter.result({
  "duration": 2551729879,
  "status": "passed"
});
formatter.scenario({
  "line": 126,
  "name": "Check-out and Check-in Date both in the past - if user enters Check-in and Checkout date in past, booking should not be saved",
  "description": "",
  "id": "error-flow---when-one-of-the-logical-constraint-is-violated-after-entering-valid-inputs,;check-out-and-check-in-date-both-in-the-past---if-user-enters-check-in-and-checkout-date-in-past,-booking-should-not-be-saved",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 125,
      "name": "@ErrorFlow"
    }
  ]
});
formatter.step({
  "line": 127,
  "name": "input Firstname as \"ErrorChkInChkOutPast\"",
  "keyword": "When "
});
formatter.step({
  "line": 128,
  "name": "input Surname as \"ErrorChkInChkOutPast\"",
  "keyword": "When "
});
formatter.step({
  "line": 129,
  "name": "input Price as \"30\"",
  "keyword": "When "
});
formatter.step({
  "line": 130,
  "name": "input Deposit as \"false\"",
  "keyword": "When "
});
formatter.step({
  "line": 131,
  "name": "input Check-in date as \"$$TODAY-20$DAYS\"",
  "keyword": "When "
});
formatter.step({
  "line": 132,
  "name": "input Check-out date as \"$$TODAY-9$DAYS\"",
  "keyword": "When "
});
formatter.step({
  "line": 133,
  "name": "save button clicked",
  "keyword": "And "
});
formatter.step({
  "line": 134,
  "name": "booking failed",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "ErrorChkInChkOutPast",
      "offset": 20
    }
  ],
  "location": "BookingFormSteps.input_firstname_as(String)"
});
formatter.result({
  "duration": 71036004,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "ErrorChkInChkOutPast",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_surname_as(String)"
});
formatter.result({
  "duration": 58530915,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "30",
      "offset": 16
    }
  ],
  "location": "BookingFormSteps.input_price_as(String)"
});
formatter.result({
  "duration": 39125773,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "false",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_Deposit_as(String)"
});
formatter.result({
  "duration": 35233493,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY-20$DAYS",
      "offset": 24
    }
  ],
  "location": "BookingFormSteps.input_Checkin_Date_as(String)"
});
formatter.result({
  "duration": 102466922,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "$$TODAY-9$DAYS",
      "offset": 25
    }
  ],
  "location": "BookingFormSteps.input_Checkout_Date_as(String)"
});
formatter.result({
  "duration": 114786519,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.save_button_clicked()"
});
formatter.result({
  "duration": 12405573257,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.booking_failed()"
});
formatter.result({
  "duration": 329551,
  "error_message": "java.lang.AssertionError: Booking should not have saved successfully but saved with Id\nExpected: is \u003c0\u003e\n     but: was \u003c7535\u003e\r\n\tat org.hamcrest.MatcherAssert.assertThat(MatcherAssert.java:20)\r\n\tat org.junit.Assert.assertThat(Assert.java:865)\r\n\tat com.equalexperts.cucumber.glue.stepdefinitions.BookingFormSteps.booking_failed(BookingFormSteps.java:114)\r\n\tat ✽.Then booking failed(TestSuit/BookingForm_ErrorFlow.feature:134)\r\n",
  "status": "failed"
});
formatter.after({
  "duration": 576745758,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "booking form page opened in browser",
  "keyword": "Given "
});
formatter.match({
  "location": "BookingFormSteps.booking_form_page_opened_in_browser()"
});
formatter.result({
  "duration": 3048191287,
  "status": "passed"
});
formatter.scenario({
  "line": 137,
  "name": "Invalid Date Format CheckIn - if user enters incorrect format for Check-in, booking should not be saved",
  "description": "",
  "id": "error-flow---when-one-of-the-logical-constraint-is-violated-after-entering-valid-inputs,;invalid-date-format-checkin---if-user-enters-incorrect-format-for-check-in,-booking-should-not-be-saved",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 136,
      "name": "@ErrorFlow"
    }
  ]
});
formatter.step({
  "line": 138,
  "name": "input Firstname as \"InvalidCheckInDt\"",
  "keyword": "When "
});
formatter.step({
  "line": 139,
  "name": "input Surname as \"InvalidCheckInDt\"",
  "keyword": "When "
});
formatter.step({
  "line": 140,
  "name": "input Price as \"10.01\"",
  "keyword": "When "
});
formatter.step({
  "line": 141,
  "name": "input Deposit as \"false\"",
  "keyword": "When "
});
formatter.step({
  "line": 142,
  "name": "input Check-in date as \"2018-16-16\"",
  "keyword": "When "
});
formatter.step({
  "line": 143,
  "name": "input Check-out date as \"2018-03-16\"",
  "keyword": "When "
});
formatter.step({
  "line": 144,
  "name": "save button clicked",
  "keyword": "And "
});
formatter.step({
  "line": 145,
  "name": "booking failed",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "InvalidCheckInDt",
      "offset": 20
    }
  ],
  "location": "BookingFormSteps.input_firstname_as(String)"
});
formatter.result({
  "duration": 58015493,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "InvalidCheckInDt",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_surname_as(String)"
});
formatter.result({
  "duration": 51643405,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "10.01",
      "offset": 16
    }
  ],
  "location": "BookingFormSteps.input_price_as(String)"
});
formatter.result({
  "duration": 75600691,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "false",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_Deposit_as(String)"
});
formatter.result({
  "duration": 69040831,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "2018-16-16",
      "offset": 24
    }
  ],
  "location": "BookingFormSteps.input_Checkin_Date_as(String)"
});
formatter.result({
  "duration": 77589402,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "2018-03-16",
      "offset": 25
    }
  ],
  "location": "BookingFormSteps.input_Checkout_Date_as(String)"
});
formatter.result({
  "duration": 108143417,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.save_button_clicked()"
});
formatter.result({
  "duration": 12332097358,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.booking_failed()"
});
formatter.result({
  "duration": 41051,
  "status": "passed"
});
formatter.after({
  "duration": 582147436,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "booking form page opened in browser",
  "keyword": "Given "
});
formatter.match({
  "location": "BookingFormSteps.booking_form_page_opened_in_browser()"
});
formatter.result({
  "duration": 2872855873,
  "status": "passed"
});
formatter.scenario({
  "line": 148,
  "name": "Invalid Date Format Checkout - if user enters incorrect format for Check-out, booking should not be saved",
  "description": "",
  "id": "error-flow---when-one-of-the-logical-constraint-is-violated-after-entering-valid-inputs,;invalid-date-format-checkout---if-user-enters-incorrect-format-for-check-out,-booking-should-not-be-saved",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 147,
      "name": "@ErrorFlow"
    }
  ]
});
formatter.step({
  "line": 149,
  "name": "input Firstname as \"InvalidCheckOutDt\"",
  "keyword": "When "
});
formatter.step({
  "line": 150,
  "name": "input Surname as \"InvalidCheckOutDt\"",
  "keyword": "When "
});
formatter.step({
  "line": 151,
  "name": "input Price as \"10.02\"",
  "keyword": "When "
});
formatter.step({
  "line": 152,
  "name": "input Deposit as \"true\"",
  "keyword": "When "
});
formatter.step({
  "line": 153,
  "name": "input Check-in date as \"2018-03-16\"",
  "keyword": "When "
});
formatter.step({
  "line": 154,
  "name": "input Check-out date as \"2018-16-16\"",
  "keyword": "When "
});
formatter.step({
  "line": 155,
  "name": "save button clicked",
  "keyword": "And "
});
formatter.step({
  "line": 156,
  "name": "booking failed",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "InvalidCheckOutDt",
      "offset": 20
    }
  ],
  "location": "BookingFormSteps.input_firstname_as(String)"
});
formatter.result({
  "duration": 83467352,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "InvalidCheckOutDt",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_surname_as(String)"
});
formatter.result({
  "duration": 111764681,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "10.02",
      "offset": 16
    }
  ],
  "location": "BookingFormSteps.input_price_as(String)"
});
formatter.result({
  "duration": 89778623,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "true",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_Deposit_as(String)"
});
formatter.result({
  "duration": 64453339,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "2018-03-16",
      "offset": 24
    }
  ],
  "location": "BookingFormSteps.input_Checkin_Date_as(String)"
});
formatter.result({
  "duration": 110967981,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "2018-16-16",
      "offset": 25
    }
  ],
  "location": "BookingFormSteps.input_Checkout_Date_as(String)"
});
formatter.result({
  "duration": 129818549,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.save_button_clicked()"
});
formatter.result({
  "duration": 12538520454,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.booking_failed()"
});
formatter.result({
  "duration": 30789,
  "status": "passed"
});
formatter.after({
  "duration": 526569560,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "booking form page opened in browser",
  "keyword": "Given "
});
formatter.match({
  "location": "BookingFormSteps.booking_form_page_opened_in_browser()"
});
formatter.result({
  "duration": 2403033013,
  "status": "passed"
});
formatter.scenario({
  "line": 159,
  "name": "Invalid Date 31st Feb Checkout - if user enters incorrect format for Check-out, booking should not be saved",
  "description": "",
  "id": "error-flow---when-one-of-the-logical-constraint-is-violated-after-entering-valid-inputs,;invalid-date-31st-feb-checkout---if-user-enters-incorrect-format-for-check-out,-booking-should-not-be-saved",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 158,
      "name": "@ErrorFlow"
    }
  ]
});
formatter.step({
  "line": 160,
  "name": "input Firstname as \"31stFebCheckOut\"",
  "keyword": "When "
});
formatter.step({
  "line": 161,
  "name": "input Surname as \"31stFebCheckOut\"",
  "keyword": "When "
});
formatter.step({
  "line": 162,
  "name": "input Price as \"10.02\"",
  "keyword": "When "
});
formatter.step({
  "line": 163,
  "name": "input Deposit as \"true\"",
  "keyword": "When "
});
formatter.step({
  "line": 164,
  "name": "input Check-in date as \"2018-01-16\"",
  "keyword": "When "
});
formatter.step({
  "line": 165,
  "name": "input Check-out date as \"2018-02-31\"",
  "keyword": "When "
});
formatter.step({
  "line": 166,
  "name": "save button clicked",
  "keyword": "And "
});
formatter.step({
  "line": 167,
  "name": "booking failed",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "31stFebCheckOut",
      "offset": 20
    }
  ],
  "location": "BookingFormSteps.input_firstname_as(String)"
});
formatter.result({
  "duration": 60770876,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "31stFebCheckOut",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_surname_as(String)"
});
formatter.result({
  "duration": 73251259,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "10.02",
      "offset": 16
    }
  ],
  "location": "BookingFormSteps.input_price_as(String)"
});
formatter.result({
  "duration": 43590491,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "true",
      "offset": 18
    }
  ],
  "location": "BookingFormSteps.input_Deposit_as(String)"
});
formatter.result({
  "duration": 33608923,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "2018-01-16",
      "offset": 24
    }
  ],
  "location": "BookingFormSteps.input_Checkin_Date_as(String)"
});
formatter.result({
  "duration": 228223285,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "2018-02-31",
      "offset": 25
    }
  ],
  "location": "BookingFormSteps.input_Checkout_Date_as(String)"
});
formatter.result({
  "duration": 107903570,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.save_button_clicked()"
});
formatter.result({
  "duration": 12512090203,
  "status": "passed"
});
formatter.match({
  "location": "BookingFormSteps.booking_failed()"
});
formatter.result({
  "duration": 281278,
  "error_message": "java.lang.AssertionError: Booking should not have saved successfully but saved with Id\nExpected: is \u003c0\u003e\n     but: was \u003c7537\u003e\r\n\tat org.hamcrest.MatcherAssert.assertThat(MatcherAssert.java:20)\r\n\tat org.junit.Assert.assertThat(Assert.java:865)\r\n\tat com.equalexperts.cucumber.glue.stepdefinitions.BookingFormSteps.booking_failed(BookingFormSteps.java:114)\r\n\tat ✽.Then booking failed(TestSuit/BookingForm_ErrorFlow.feature:167)\r\n",
  "status": "failed"
});
formatter.after({
  "duration": 531295790,
  "status": "passed"
});
});