<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1521322468477" ID="ID_1808757321" MODIFIED="1521332094345" TEXT="Testing HotelBooking">
<node CREATED="1521322524850" ID="ID_1869654209" MODIFIED="1521327741201" POSITION="right" TEXT="Functional Testing">
<node CREATED="1521322557553" ID="ID_1587936559" MODIFIED="1521322679426" TEXT="Basic Flow">
<node CREATED="1521323859621" ID="ID_217385388" MODIFIED="1521323922342" TEXT="Add new booking"/>
<node CREATED="1521323925550" ID="ID_851147083" MODIFIED="1521323934862" TEXT="Delete booking"/>
<node CREATED="1521323937798" ID="ID_287315872" MODIFIED="1521323951181" TEXT="Delete all bookings on the page"/>
<node CREATED="1521325161891" ID="ID_908395373" MODIFIED="1521325198807" TEXT="Data persisted and retrieved between sessions"/>
</node>
<node CREATED="1521322720158" ID="ID_577323772" MODIFIED="1521322730893" TEXT="Exception Flow">
<node CREATED="1521323964002" ID="ID_1774477554" MODIFIED="1521323980150" TEXT="Add Duplicate booking"/>
<node CREATED="1521324074208" ID="ID_45111912" MODIFIED="1521324115527" TEXT="booking with negative price"/>
<node CREATED="1521324124363" ID="ID_1086805415" MODIFIED="1521324148913" TEXT="booking with non numeric price"/>
<node CREATED="1521324602158" ID="ID_1224696640" MODIFIED="1521324718889" TEXT="CheckoutDate earlier than CheckInDate"/>
<node CREATED="1521324633394" ID="ID_1774116176" MODIFIED="1521324648897" TEXT="CheckinDate and CheckoutDate in past"/>
<node CREATED="1521324870220" ID="ID_63976299" MODIFIED="1521324955482" TEXT="Date Format other than YYYY-MM-DD">
<node CREATED="1521324887289" ID="ID_583077288" MODIFIED="1521324938637" TEXT="Checkin Date"/>
<node CREATED="1521324887289" ID="ID_1956360670" MODIFIED="1521324946060" TEXT="Checkin Date"/>
</node>
</node>
<node CREATED="1521323306289" ID="ID_1584334845" MODIFIED="1521323331666" TEXT="Form Input fields Testing">
<node CREATED="1521322900233" ID="ID_760309399" MODIFIED="1521323431917" TEXT="Firstname">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Assumption
    </p>
    <ol>
      <li>
        MAX allowed length 50 char
      </li>
      <li>
        Special Char not allowed
      </li>
      <li>
        Null not allowed
      </li>
    </ol>
  </body>
</html></richcontent>
<icon BUILTIN="info"/>
<node CREATED="1521322689170" ID="ID_1761112049" MODIFIED="1521324560788" TEXT="Boundray Value Analysis (BVA)">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Boundary Value Tests Performed:
    </p>
    <ol>
      <li>
        firstname (empty)
      </li>
      <li>
        1 char firstname (Single)
      </li>
      <li>
        50 Char in firstname
      </li>
      <li>
        51 Char in firstname
      </li>
    </ol>
  </body>
</html></richcontent>
</node>
<node CREATED="1521323475314" ID="ID_1822897821" MODIFIED="1521323491574" TEXT="Special Char Testing"/>
</node>
<node CREATED="1521322918444" ID="ID_467250953" MODIFIED="1521323412566" TEXT="Surname">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Assumption
    </p>
    <ol>
      <li>
        MAX allowed length 50 char
      </li>
      <li>
        Special Char not allowed
      </li>
      <li>
        Null not allowed
      </li>
    </ol>
  </body>
</html></richcontent>
<node CREATED="1521322689170" ID="ID_624073948" MODIFIED="1521324568909" TEXT="Boundray Value Analysis (BVA)">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Boundary Value Tests Performed:
    </p>
    <ol>
      <li>
        surname (empty)
      </li>
      <li>
        1 char surname (Single)
      </li>
      <li>
        50 Char in surname
      </li>
      <li>
        51 Char in surname
      </li>
    </ol>
  </body>
</html></richcontent>
</node>
<node CREATED="1521323475314" ID="ID_955148927" MODIFIED="1521323491574" TEXT="Special Char Testing"/>
</node>
<node CREATED="1521322933889" ID="ID_1361601817" MODIFIED="1521324397314" TEXT="Price">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Assumption
    </p>
    <ol>
      <li>
        MAX 99999
      </li>
      <li>
        4 digits after decimal points
      </li>
      <li>
        Char not allowed
      </li>
      <li>
        Null not allowed
      </li>
    </ol>
  </body>
</html></richcontent>
<node CREATED="1521322689170" ID="ID_276846174" MODIFIED="1521324468210" TEXT="Boundray Value Analysis (BVA)">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Boundary Value Tests Performed: with price
    </p>
    <ol>
      <li>
        -1
      </li>
      <li>
        0 price (empty)
      </li>
      <li>
        0.00
      </li>
      <li>
        1.1
      </li>
      <li>
        1.10
      </li>
      <li>
        10.95
      </li>
      <li>
        99998.9999
      </li>
      <li>
        99999.0
      </li>
    </ol>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1521322947856" ID="ID_1394887448" MODIFIED="1521323629141" TEXT="deposit">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Assumption
    </p>
    <ol>
      <li>
        Only allowed values true or false
      </li>
    </ol>
  </body>
</html></richcontent>
<node CREATED="1521322689170" ID="ID_537097553" MODIFIED="1521324498399" TEXT="Boundray Value Analysis (BVA)">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Boundary Value Tests Performed: with deposit value
    </p>
    <ol>
      <li>
        true
      </li>
      <li>
        false
      </li>
    </ol>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1521322953948" ID="ID_1302780260" MODIFIED="1521323713434" TEXT="Checkin Date">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Assumption
    </p>
    <ol>
      <li>
        date could be in past or in future
      </li>
      <li>
        format is YYYY-MM-DD
      </li>
    </ol>
  </body>
</html></richcontent>
<node CREATED="1521322689170" ID="ID_760173520" MODIFIED="1521324840729" TEXT="Boundray Value Analysis (BVA)">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Boundary Value Tests Performed: with Checkin date value
    </p>
    <ol>
      <li>
        Date in Past
      </li>
      <li>
        Date is today, now
      </li>
      <li>
        Date in Future
      </li>
    </ol>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1521322965766" ID="ID_1818486734" MODIFIED="1521323840243" TEXT="Checkout Date">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Assumption
    </p>
    <ol>
      <li>
        date should be in future
      </li>
      <li>
        format is YYYY-MM-DD
      </li>
    </ol>
  </body>
</html></richcontent>
<node CREATED="1521322689170" ID="ID_1055637048" MODIFIED="1521324862195" TEXT="Boundray Value Analysis (BVA)">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Boundary Value Tests Performed: with Checkout date value
    </p>
    <ol>
      <li>
        Date in Past
      </li>
      <li>
        Date is today, now
      </li>
      <li>
        Date in Future
      </li>
    </ol>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node CREATED="1521322741353" ID="ID_1592283660" MODIFIED="1521327775201" POSITION="right" TEXT="GUI Testing">
<node CREATED="1521323715492" ID="ID_772353318" MODIFIED="1521325098126" TEXT="Page title"/>
<node CREATED="1521323740096" ID="ID_1902763169" MODIFIED="1521323757341" TEXT="Element position"/>
<node CREATED="1521323762970" ID="ID_1918618357" MODIFIED="1521323796500" TEXT="Element identifier"/>
<node CREATED="1521324971466" ID="ID_1685953979" MODIFIED="1521325092562" TEXT="Calender element testing"/>
<node CREATED="1521324990786" ID="ID_1980451820" MODIFIED="1521325014634" TEXT="Default field focus"/>
<node CREATED="1521325021229" ID="ID_207502728" MODIFIED="1521325086619" TEXT="Page navigation using keyboard keys"/>
</node>
<node CREATED="1521332096842" ID="ID_938560139" MODIFIED="1521332113261" POSITION="left" TEXT="Non Functional Testing">
<node CREATED="1521330139962" ID="ID_558658026" MODIFIED="1521330155900" TEXT="CrossBrowser Testing">
<node CREATED="1521330186741" ID="ID_1714671793" MODIFIED="1521330194758" TEXT="Internet Explorer"/>
<node CREATED="1521330199482" ID="ID_1490799619" MODIFIED="1521330206913" TEXT="firefox"/>
<node CREATED="1521330212163" ID="ID_1722136870" MODIFIED="1521330215081" TEXT="chrome"/>
<node CREATED="1521330221572" ID="ID_1467397071" MODIFIED="1521330224108" TEXT="safari"/>
<node CREATED="1521339813497" ID="ID_1409452117" MODIFIED="1521339826107" TEXT="headless\phantomjs"/>
</node>
<node CREATED="1521325933745" ID="ID_564818138" MODIFIED="1521405900403" TEXT="Security Testing">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Not yet planned
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="info"/>
</node>
<node CREATED="1521322591109" ID="ID_1527824353" MODIFIED="1521322871900" TEXT="Performance Testing">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Not yet planned
    </p>
  </body>
</html></richcontent>
<icon BUILTIN="info"/>
</node>
<node CREATED="1521405722401" ID="ID_1093303393" MODIFIED="1521405896220" TEXT="Device Testing">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Not Tested
    </p>
    <p>
      
    </p>
  </body>
</html>
</richcontent>
<icon BUILTIN="info"/>
<node CREATED="1521405733550" ID="ID_450680328" MODIFIED="1521405740250" TEXT="Desktop">
<node CREATED="1521405968801" ID="ID_1436081713" MODIFIED="1521405986561" TEXT="windows"/>
<node CREATED="1521405992449" ID="ID_1237396784" MODIFIED="1521405996661" TEXT="mac"/>
<node CREATED="1521405998742" ID="ID_1196049737" MODIFIED="1521406005841" TEXT="linux\unix"/>
</node>
<node CREATED="1521405743580" ID="ID_929854657" MODIFIED="1521405800569" TEXT="phone">
<node CREATED="1521405803305" ID="ID_879018656" MODIFIED="1521405813338" TEXT="android"/>
<node CREATED="1521405815233" ID="ID_612040761" MODIFIED="1521405817644" TEXT="iOs"/>
<node CREATED="1521405823346" ID="ID_617043315" MODIFIED="1521405839608" TEXT="Windows"/>
</node>
<node CREATED="1521405756325" ID="ID_953932890" MODIFIED="1521405867650" TEXT="Others">
<node CREATED="1521405869733" ID="ID_654475597" MODIFIED="1521405874941" TEXT="Xbox"/>
</node>
<node CREATED="1521405743580" ID="ID_901618663" MODIFIED="1521405854936" TEXT="tablets">
<node CREATED="1521405803305" ID="ID_1772559488" MODIFIED="1521405813338" TEXT="android"/>
<node CREATED="1521405815233" ID="ID_1435196497" MODIFIED="1521405817644" TEXT="iOs"/>
<node CREATED="1521405823346" ID="ID_1701502128" MODIFIED="1521405839608" TEXT="Windows"/>
</node>
</node>
</node>
</node>
</map>
